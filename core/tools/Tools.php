<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/10 0010
 * Time: 16:33
 */

namespace tools;


class Tools
{
    public static function getNowTime()
    {
        return time();
    }

    public static function getTodayTime()
    {
        return strtotime(date("Y/m/d"));
    }
    public static function get_msectime(){
        list($msec, $sec) = explode(' ', microtime());
        $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
        return $msectime;
    }

    /**
     * 获取配置信息
     * @param string $tag 配置标识
     * @param string $field 字段名称
     * @param mixed $default 默认值
     * @return mixed
     */
    public static function getYaconfConfig(string $tag, string $field = '', $default = null)
    {
        //这里啥都没有的
        $configs = \Yaconf::get($tag . '.base');
        if (is_null($configs)) {
            $tag0 = explode(".", $tag);
            $configs = \Yaconf::get($tag0[0] . '.base');
            if (is_null($configs)) {
                return $default;
            }
        }
        if (is_array($configs) && (strlen($field) > 0)) {
            return self::getArrayVal($configs, $field, $default);
        } else {
            return $configs;
        }
    }

    /**
     * 判断字符串是否为 Json 格式
     *
     * @param string $data Json 字符串
     * @param bool $assoc 是否返回关联数组。默认返回对象
     *
     * @return array|bool|object 成功返回转换后的对象或数组，失败返回 false
     */
    public static function json_decode($text = '', $assoc = true)
    {
        if(is_string($text)){
            $text = preg_replace('/\s+/', '', $text);
            $text = preg_replace('/\:\d+/', '"\0"', $text);
            $text = preg_replace('/"":/', '":"', $text);
            $outText = str_replace(array("\r\n", "\r", "\n"), "", $text);
            $data = json_decode($outText, $assoc);
            if (($data && is_object($data)) || (is_array($data) && !empty($data))) {
                return $data;
            }
        }elseif(is_array($text)){
            return $text;
        }elseif (is_object($text)){
            return $text;
        }
        return [];
    }

    public static function json_encode($json)
    {
        return json_encode($json);
    }

    public static function get_url($url, $aHeader)
    {
        $headerArray = array("Content-type:application/json;", "Accept:application/json", ' Accept-Encoding: gzip, deflate');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if (!empty($aHeader)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeader);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function post_url($url, $data, $header = array(), $timeout = 5)
    {
        $data = json_encode($data);
        $headerArray = array("Content-type:application/json;charset='utf-8'", "Accept:application/json");
        $header = array_merge($header, $headerArray);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        // 超时设置,以秒为单位
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        //curl_setopt($ch, CURLOPT_NOSIGNAL, 1);     //注意，毫秒超时一定要设置这个
        //curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200); //超时毫秒，cURL 7.16.2中被加入。从PHP 5.2.3起可使用
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }

    public static function put_url($url, $data, $timeout = 5)
    {
        $data = json_encode($data);
        $ch = curl_init(); //初始化CURL句柄
        curl_setopt($ch, CURLOPT_URL, $url); //设置请求的URL
        // 超时设置,以秒为单位
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        //curl_setopt($ch, CURLOPT_NOSIGNAL, 1);     //注意，毫秒超时一定要设置这个
        //curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200); //超时毫秒，cURL 7.16.2中被加入。从PHP 5.2.3起可使用
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //设为TRUE把curl_exec()结果转化为字串，而不是直接输出
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); //设置请求方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);//设置提交的字符串
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function del_url($url, $data, $timeout = 5)
    {
        $data = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // 超时设置,以秒为单位
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        //curl_setopt($ch, CURLOPT_NOSIGNAL, 1);     //注意，毫秒超时一定要设置这个
        //curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200); //超时毫秒，cURL 7.16.2中被加入。从PHP 5.2.3起可使用
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        //        return json_decode($output, true);
        return $output;
    }

    public static function patch_url($url, $data,$timeout=5)
    {
        $data = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        // 超时设置,以秒为单位
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        //curl_setopt($ch, CURLOPT_NOSIGNAL, 1);     //注意，毫秒超时一定要设置这个
        //curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200); //超时毫秒，cURL 7.16.2中被加入。从PHP 5.2.3起可使用
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);     //20170611修改接口，用/id的方式传递，直接写在url中了
        $output = curl_exec($ch);
        curl_close($ch);
        //        return json_decode($output, true);
        return $output;
    }

    public static function create_guid($data = '')
    {

        static $guid = '';
        $uid = uniqid("", true);
        $hash = strtoupper(hash('ripemd128', $uid . $guid . md5($data)));
        $guid = '{' .
            substr($hash, 0, 8) .
            '-' .
            substr($hash, 8, 4) .
            '-' .
            substr($hash, 12, 4) .
            '-' .
            substr($hash, 16, 4) .
            '-' .
            substr($hash, 20, 12) .
            '}';
        return $guid;

    }


    /**
     * 发送HTTP请求方法
     * @param string $url 请求URL
     * @param array $params 请求参数
     * @param string $method 请求方法GET/POST
     * @return array  $data   响应数据
     */
    public static function httpCurl($url, $params, $method = 'POST', $header = array(), $timeout = 30,$multi = false)
    {
        $header = ($header && is_array($header)) ? $header : array();
        date_default_timezone_set('PRC');
        $opts = array(
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_COOKIESESSION => true,
            CURLOPT_FOLLOWLOCATION => 1,
            CURLOPT_COOKIE => session_name() . '=' . session_id(),
        );

        /* 根据请求类型设置特定参数 */
        switch (strtoupper($method)) {
            case 'GET':
                if (!empty($params)) {
                    $opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
                }
                // $opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
                // 链接后拼接参数  &  非？

                break;
            case 'POST':
                //判断是否传输文件
                $params = $multi ? $params : http_build_query($params);
                $opts[CURLOPT_URL] = $url;
                $opts[CURLOPT_POST] = 1;
                $opts[CURLOPT_POSTFIELDS] = $params;
                break;
            default:
                throw new Exception('Unsupported request mode!');
        }

        /* 初始化并执行curl请求 */
        try {
            $ch = curl_init();
            curl_setopt_array($ch, $opts);
            $data = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);
            if ($error) {
                throw new Exception($error);
            }
            return $data;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return '';
    }

    /**
     * 发送GET请求
     * @param string $url 请求地址
     * @param int $timeout 超时时间,单位为毫秒
     * @param array $extends 扩展数组
     * @return mixed
     */
    public static function sendGetReq(string $url, int $timeout = 2000, array &$extends = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//        if(isset($extends['req_headers']) && is_array($extends['req_headers'])){
//            curl_setopt($ch, CURLOPT_HTTPHEADER, $extends['req_headers']);
//        } else {
//            curl_setopt($ch, CURLOPT_HTTPHEADER, false);
//        }

        $data = curl_exec($ch);
        $errorNo = curl_errno($ch);
        $errorInfo = curl_error($ch);
        $extendData['send_head'] = curl_getinfo($ch);
        curl_close($ch);
        return $data;
    }

    public static function send_post($url, $post_data)
    {
        $postdata = http_build_query($post_data);
        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type:application/x-www-form-urlencoded',
                'content' => $postdata,
                'timeout' => 15 * 60 // 超时时间（单位:s）
            )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }

    public static function file_get($url, $header)
    {
        //请求地址 $url
        //headers请求头
        $context_options = array(
            'http' =>
                array(
                    'method' => "GET",
                    'header' => $header,
                ));
        $context = stream_context_create($context_options);
        //获取资源
        $result = file_get_contents($url, FALSE, $context);
        return $result;
    }

    public static function curl_get($url, $header, $timeout = 5)
    {
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);
        // 超时设置,以秒为单位
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        //curl_setopt($curl, CURLOPT_NOSIGNAL, 1);     //注意，毫秒超时一定要设置这个
        //curl_setopt($curl, CURLOPT_TIMEOUT_MS, 200); //超时毫秒，cURL 7.16.2中被加入。从PHP 5.2.3起可使用

        // 设置请求头
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        //执行命令
        $data = curl_exec($curl);

        // 显示错误信息
        if (curl_error($curl)) {
            $error = __FILE__.' : '.__LINE__."Error: " . (string)curl_error($curl);
            outLog($error);
            return $error;
        } else {
            // 打印返回的内容
            curl_close($curl);
        }
        return $data;
    }


    public static function get_server_ips()
    {
        exec('ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk \'{print $2}\'|tr -d "addr:"', $arr);
        return $arr;
    }

    /**
     * * 创建一个妙级的订单号
     * @return string
     * USER: Administrator
     * TIME: 2020/4/10 0010 11:17
     */
    public static function createSimpleOrderSn(){
        //秒级
        $str = date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        return $str;
    }

    /**
     * 创建一个微秒级的订单号
     * 适合放到一些大型电商网站上使用，可以处理微秒级的订单单号不重复
     * 生成24位唯一订单号码，格式：YYYY-MMDD-HHII-SS-NNNN,NNNN-CC，
     * 其中：YYYY=年份，MM=月份，DD=日期，HH=24格式小时，II=分，SS=秒，NNNNNNNN=随机数，CC=检查码
     * @return string
     * USER: Administrator
     * TIME: 2020/4/10 0010 11:17
     */
    public static function creaeteOrderSnMis(){
        @date_default_timezone_set("PRC");
        //订单号码主体（YYYYMMDDHHIISSNNNNNNNN）
        $order_id_main = date('YmdHis') . rand(10000000, 99999999);
        //订单号码主体长度
        $order_id_len = strlen($order_id_main);
        $order_id_sum = 0;
        for ($i = 0; $i < $order_id_len; $i++) {
            $order_id_sum += (int)(substr($order_id_main, $i, 1));
        }
        //唯一订单号码（YYYYMMDDHHIISSNNNNNNNNCC）
        $order_id = $order_id_main . str_pad((100 - $order_id_sum % 100) % 100, 2, '0', STR_PAD_LEFT);
        return $order_id;
    }

    public static function createOrderSnWord(){
        $yCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        $orderSn = $yCode[intval(date('Y')) - 2011] . strtoupper(dechex(date('m'))) . date('d') . substr(time(), -5) . substr(microtime(), 2, 5) . sprintf('%02d', rand(0, 99));
        return $orderSn;
    }


    public static function tail($file,$num){
        $fp = fopen($file,"r");
        $pos = -2;
        $eof = "";
        $head = false;   //当总行数小于Num时，判断是否到第一行了
        $lines = array();
        while($num>0){
            while($eof != "\n"){
                if(fseek($fp, $pos, SEEK_END)==0){    //fseek成功返回0，失败返回-1
                    $eof = fgetc($fp);
                    $pos--;
                }else{                               //当到达第一行，行首时，设置$pos失败
                    fseek($fp,0,SEEK_SET);
                    $head = true;                   //到达文件头部，开关打开
                    break;
                }
            }
            array_unshift($lines,fgets($fp));
            if($head){ break; }                 //这一句，只能放上一句后，因为到文件头后，把第一行读取出来再跳出整个循环
            $eof = "";
            $num--;
        }
        fclose($fp);
        return $lines;
    }

    public static function array_to_lower($weChatArr)
    {
        foreach ($weChatArr as $key => $weChat) {
            $byteArr2D[] = str_split(trim($weChat));
            foreach ($byteArr2D[$key] as $byte) {
                $byteToLowerArr2D[$key][] = ord($byte) >= 65 && ord($byte) <= 90 ? chr(ord($byte) + 32) : $byte;
            }
        }
        return array_map('implode', $byteToLowerArr2D);
    }


    public static function asynchronous_get($url,$header='',$ignoreResults=true){
        $urlArr = parse_url($url);
        $scheme = isset($urlArr['scheme']) ? $urlArr['scheme'] : 'http';
        $port = isset($urlArr['port']) ? $urlArr['port'] : 80;
        $hostname = $urlArr['host'];
        $path = $urlArr['path'];
        $domain = $scheme.'://'.$hostname;
        $fp = fsockopen($hostname, $port, $errno, $errstr, 30);
        $result = '';
        if (!$fp) {
            echo "$errstr ($errno)<br />\n";
        } else {
            $out = "GET $path  / HTTP/1.1\r\n";
            $out .= "Host: $hostname\r\n";
            if(!empty($header)){
                $out .= $header."\r\n";
            }
            $out .= "Connection: Close\r\n\r\n";
            fwrite($fp, $out);
            /*忽略执行结果*/
            if(!$ignoreResults){
                while (!feof($fp)) {
                    $result .= fgets($fp, 128);
                }
            }
            fclose($fp);
        }
        return $result;
    }
}
