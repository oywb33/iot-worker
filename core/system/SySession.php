<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/26 0026
 * Time: 18:22
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\system;


use tools\Tools;

class SySession
{
    public $session_id = '';
    public $var_path = APP_PATH.'/var/';
    public $session_info = [];

    public static function clearSession(){
        $session = new SySession();
        foreach(glob($session->var_path.'*.session') as $start_file)
        {
            if(file_exists($start_file)){
                unlink($start_file);
            }
        }
    }


    public static function open(string $session_id)
    {
        $session = new SySession();
        $session->session_id = $session_id;
        $session->session_info = ['init' => time()];
        $session->save();
        $session_file = $session->var_path . md5($session->session_id) . '.session';
        outLog('save session_file ' . $session_file);
        return $session_file;
    }

    public static function setSession(string $session_id, array $session_info)
    {
        $session = new SySession();
        $session->session_id = $session_id;
        $session->session_info = $session_info;
        $session->save();
        return $session;
    }

    public function __construct($path='')
    {
        if(!empty($path)){
            $this->var_path = $path;
        }
        if (!file_exists($this->var_path)) {
            mkdir($this->var_path);
            chmod($this->var_path, 0644);
        }
    }

    public static function getSession(string $session_id, $key = null)
    {
        $session = new SySession();
        $session->session_id = $session_id;
        return $session->get($key);
    }

    public function get($key = null)
    {
        if (empty($this->session_id)) {
            return false;
        }
        $session_file = $this->var_path . md5($this->session_id) . '.session';
        if (file_exists($session_file)) {
            $session_file_size = filesize($session_file);
            if ($session_file_size > 0) {
                $handle = fopen($session_file, "r");//读取二进制文件时，需要将第二个参数设置成'rb'
                $contents = fread($handle, filesize($session_file));
                fclose($handle);
            } else {
                $contents = [];
            }
            $obj = Tools::json_decode($contents, true);
            if (!empty($key)) {
                return isset($obj[$key]) ? $obj[$key] : null;
            }
            return $obj;
        }
        return null;
    }


    public function save()
    {
        try {
            if (empty($this->session_id)) {
                return false;
            }
            $session_file = $this->var_path . md5($this->session_id) . '.session';
            $contents = '';
            if (file_exists($session_file)) {
                $session_file_size = filesize($session_file);
                if ($session_file_size > 0) {
                    $handle = fopen($session_file, "r");//读取二进制文件时，需要将第二个参数设置成'rb'
                    $contents = fread($handle, $session_file_size);
                    fclose($handle);
                }
            }
            if(empty($contents)){
                $obj = $this->session_info;
            } else {
                $obj = Tools::json_decode($contents, true);
                if ( !empty($contents) && is_array($obj)) {
                    foreach ($this->session_info as $key => $val) {
                        $obj[$key] = $val;
                    }
                }
            }
            $obj['session_updated'] = Tools::getNowTime();
            $myfile = fopen($session_file, "w") or die("Unable to open file!");

            fwrite($myfile, Tools::json_encode($obj));
            fclose($myfile);
        } catch (\Exception $e) {
            outLog($e->getMessage());
        }
    }

    public static function clear(string $session_id)
    {
        $session = new SySession();
        $session->session_id = $session_id;
        $session->close();
    }

    public function close()
    {
        if (empty($this->session_id)) {
            return false;
        }
        $session_file = $this->var_path . md5($this->session_id) . '.session';
        delFile($session_file);
        return $this;
    }
}