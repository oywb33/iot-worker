<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/10 0010
 * Time: 17:13
 */
namespace app\system\Factory;


class RedisFactory
{

    private static $_instance; //存储对象
    private function __construct( ){
        $config = getSysConfig('redis');
        self::$_instance = new \Redis();
        //从配置读取
        self::$_instance->pconnect($config['host'], $config['port']);
        self::$_instance->auth($config['password']);
    }


    /**
     * 实现2
     * @return \Redis
     */
    public static function getInstance( )
    {
        if (!self::$_instance) {
            new self();
        }
        else{
            try {
                @trigger_error('flag', E_USER_NOTICE);
                self::$_instance->ping();
                $error = error_get_last();
                if($error['message'] != 'flag')
                    throw new \Exception('Redis server went away');
            } catch (\Exception $e) {
                outLog(__FILE__.' | '.__LINE__.' | '.$e->getMessage().' | '.$e->getTraceAsString());
                // 断线重连
                new self();
            }
        }
        return self::$_instance;
    }

    /**
     * 禁止clone
     */
    private function __clone(){}

    /**
     * 其他方法自动调用
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method,$args)
    {
        return call_user_func_array([self::$_instance, $method], $args);
    }

    /**
     * 静态调用
     * @param $method
     * @param $args
     * @return mixed
     */
    public static function __callStatic($method,$args)
    {
        self::getInstance();
        return call_user_func_array([self::$_instance, $method], $args);
    }


    /**
     * 在非workerman环境向队列发送消息
     * $queue = 'user-1';
     * $data = ['some', 'data'];
     * RedisFactory::queue_send($queue,$data);
     * @param $redis
     * @param $queue
     * @param $data
     * @param int $delay
     * @return mixed
     */
    public static function queue_send($queue, $data, $delay = 0) {
        $redis = RedisFactory::getInstance();
        $queue_waiting = 'redis-queue-waiting';
        $queue_delay = 'redis-queue-delayed';
        $now = time();
        $package_str = json_encode([
            'id'       => rand(),
            'time'     => $now,
            'delay'    => 0,
            'attempts' => 0,
            'queue'    => $queue,
            'data'     => $data
        ]);
        if ($delay) {
            return $redis->zAdd($queue_delay, $now + $delay, $package_str);
        }
        return $redis->lPush($queue_waiting.$queue, $package_str);
    }

}
