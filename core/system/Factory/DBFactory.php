<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/10 0010
 * Time: 17:15
 */
namespace app\system\Factory;


use Log\Traits\SimpleTrait;
use system\pools\DbPool;

class DBFactory
{
    use SimpleTrait;
    private static $_instance; //存储对象

    public static function getInstance(){
        if (!self::$_instance) {
            self::$_instance = DbPool::getInstance();
        }
        return self::$_instance;
    }


    /**
     * 禁止clone
     */
    private function __clone(){}
    /**
     * 其他方法自动调用
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method,$args)
    {
        return call_user_func_array([self::$_instance, $method], $args);
    }

    /**
     * 静态调用
     * @param $method
     * @param $args
     * @return mixed
     */
    public static function __callStatic($method,$args)
    {
        self::getInstance();
        return call_user_func_array([self::$_instance, $method], $args);
    }

    public static function close(){
        global $db;
        try{
            $db->close();
        }catch (\Exception $e){
            outLog('DB close  ('.__LINE__.') '.$e->getMessage());
        }
        return [];
    }
}