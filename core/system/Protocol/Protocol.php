<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/5/18 0018
 * Time: 11:02
 */

namespace app\system\Protocol;


interface Protocol
{
    public function SetDeviceID($device_id);
    public function GetDeviceId();
    public function SetProtocolType($protocol_type);
    public function GetProtocolType();
    public function SetDataType($data_type);
    public function GetDataType();
    public function SetPackage($package);
    public function GetPackage();
    public function AnalyticPackage();
    public function Run();
}