<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/12 0012
 * Time: 14:45
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\system\Protocol;


class Client
{
    public $client_id = null;
    public $gateway_port =null;
    public $RemoteIp = null;
    public $RemotePort = null;
    public $gateway_addr = null;

    public function __construct($client_id,$gateway_port = null,$RemoteIp = null,$RemotePort = null,$gateway_addr = null)
    {
        $this->client_id = $client_id;
        $this->gateway_port = $gateway_port;
        $this->RemoteIp = $RemoteIp;
        $this->RemotePort = $RemotePort;
        $this->gateway_addr = $gateway_addr;
    }


    /**
     * @return null
     */
    public function getClientId()
    {
        return $this->client_id;
    }

    /**
     * @param null $client_id
     */
    public function setClientId($client_id): void
    {
        $this->client_id = $client_id;
    }

    /**
     * @return string
     */
    public function getGatewayAddr(): string
    {
        return $this->gateway_addr;
    }

    /**
     * @return string
     */
    public function getGatewayPort(): string
    {
        return $this->gateway_port;
    }

    /**
     * @return string
     */
    public function getRemoteIp(): string
    {
        return $this->RemoteIp;
    }

    /**
     * @return string
     */
    public function getRemotePort(): string
    {
        return $this->RemotePort;
    }

    /**
     * @param string $gateway_addr
     */
    public function setGatewayAddr(string $gateway_addr): void
    {
        $this->gateway_addr = $gateway_addr;
    }


    /**
     * @param string $gateway_port
     */
    public function setGatewayPort(string $gateway_port): void
    {
        $this->gateway_port = $gateway_port;
    }

    /**
     * @param string $RemoteIp
     */
    public function setRemoteIp(string $RemoteIp): void
    {
        $this->RemoteIp = $RemoteIp;
    }

    /**
     * @param string $RemotePort
     */
    public function setRemotePort(string $RemotePort): void
    {
        $this->RemotePort = $RemotePort;
    }


}