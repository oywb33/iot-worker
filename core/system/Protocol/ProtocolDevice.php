<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/5/18 0018
 * Time: 10:43
 */

namespace app\system\Protocol;


class ProtocolDevice implements Protocol
{
    public $device_id = '';
    public $protocol_type = 'TCP';
    public $data_type = 'string';
    public $package = null;

    public function SetDeviceID($device_id){
        $this->device_id = $device_id;
    }
    public function GetDeviceId(){
        return $this->getDeviceId();
    }
    public function SetProtocolType($protocol_type){
        $this->protocol_type = $protocol_type;
    }
    public function GetProtocolType(){
        return $this->protocol_type;
    }
    public function SetDataType($data_type){
        $this->data_type = $data_type;
    }
    public function GetDataType(){
        return $this->data_type;
    }
    public function SetPackage($package){
        $this->package = $package;
    }

    public function GetPackage(){
        return $this->package;
    }

    /**
     * 解析包
     */
    public function AnalyticPackage(){

    }


    public function Run()
    {
        // TODO: Implement Run() method.
    }
}