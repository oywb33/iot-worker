<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/19 0019
 * Time: 15:59
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\system\Protocol;


class Pack
{
    public $data = null;
    public $protocol_ename = '';
    public $protocol_name = '';
    public $action = '';
}