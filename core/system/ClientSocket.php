<?php

namespace system;

use mysql_xdevapi\Exception;

class ClientSocket
{

    private $receiveTimeOutSec = 10;
    private $sendTimeOutSec = 6;
    //服务端主机
    private $serverHost = '127.0.0.1';
    //服务端端口
    private $serverPort = 9102;
    //缓冲区长度(能存放多少个客户端请求)
    public $receiveStrLength = 1024;
    public $clientSocket = null;

    public function setServerHost($host)
    {
        $this->serverHost = $host;
    }

    public function setServerPort($port)
    {
        $this->serverPort = $port;
    }

    public function setReceiveStrLength($strLength = 1024)
    {
        $this->receiveStrLength = $strLength;
    }

    public function listen($host,$port,$strLength=1024){
        $this->setServerHost($host);
        $this->setServerPort($port);
        $this->setReceiveStrLength($strLength);
    }
    public function runAll(){
        if(empty($this->serverHost) || empty($this->serverPort) || empty($this->receiveStrLength)){
            throw new \Exception('没有监听信息');
        }
        $this->connect();
    }
    /**
     * 创建一个socket套接流
     */
    public function socket_create()
    {
        $this->clientSocket = \socket_create(AF_INET, SOCK_STREAM, SOL_TCP) OR DIE('client create fail:' . \socket_strerror(\socket_last_error()));
        /****************设置socket连接选项*************/
        //接收套接流的最大超时时间1秒，后面是微秒单位超时时间，设置为零，表示不管它
        \socket_set_option($this->clientSocket, SOL_SOCKET, SO_RCVTIMEO, array("sec" => $this->receiveTimeOutSec, "usec" => 0));
        //发送套接流的最大超时时间为6秒
        \socket_set_option($this->clientSocket, SOL_SOCKET, SO_SNDTIMEO, array("sec" => $this->sendTimeOutSec, "usec" => 0));
        /****************设置socket连接选项*************/
    }

    public function test()
    {

        $this->connect();

        $toSendMessage = 'this is a test text';
        //转为GBK编码，处理乱码问题，这要看你的编码情况而定，每个人的编码都不同
        //$toSendMessage = mb_convert_encoding($toSendMessage,'GBK','UTF-8');
        //向服务端写入字符串信息

        $this->send($toSendMessage);

        echo 'client write success' . PHP_EOL;

        //读取服务端返回来的套接流信息
        while ($returnMessage = $this->read()) {
            echo 'server return message is:' . PHP_EOL . $returnMessage;
        }

        $this->close();
    }

    public function send($toSendMessage)
    {
        socket_write($this->clientSocket, $toSendMessage, strlen($toSendMessage)) OR DIE('client write fail:' . socket_strerror(socket_last_error()));
    }

    /**
     * 连接服务端的套接流，这一步就是使客户端与服务器端的套接流建立联系
     */
    public function connect()
    {
        if (empty($this->clientSocket)) {
            $this->socket_create();
        }
        //
        socket_connect($this->clientSocket, $this->serverHost, $this->serverPort) OR DIE('client connect fail:' . socket_strerror(socket_last_error()));
    }

    public function read()
    {
        ini_set("display_errors", 0);
        $return = '';
        try {
            $return = socket_read($this->clientSocket, $this->receiveStrLength);
        } catch (\Exception $e) {
            echo 'Exception => ' . $e->getMessage();
        }
        return $return;
    }

    /**
     * 工作完毕，关闭套接流
     */
    public function close()
    {
        socket_close($this->clientSocket);
    }

}