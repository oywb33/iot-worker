<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/10 0010
 * Time: 16:46
 */

namespace system;

class DB{
    public $db_PDO;
    public function __construct()
    {
    }
    public static function getInstance(){
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
            // error was suppressed with the @-operator
            if (0 === error_reporting()) {
                return false;
            }
            throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
        }, E_WARNING);

        global $conn;
        if(is_null($conn)){
            try {
                //默认这个不是长连接，如果需要数据库长连接，需要最后加一个参数：array(PDO::ATTR_PERSISTENT => true)
                $conn = new \PDO("mysql:host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USER, DB_PWD,array(\PDO::ATTR_PERSISTENT => true, \PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true, \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8';",\PDO::ATTR_EMULATE_PREPARES => true,));
                $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                return $conn;
            }
            catch(\PDOException $e)
            {
                outLog('getInstance PDOException: ['.__FILE__.__LINE__.$e->getMessage().']');
            }
        }
        try{
            if (!self::pdo_ping($conn) ) {
                outLog('db reconnect');
                $conn = null;
                sleep(1);
                return self::getInstance();
            }
        }catch (\Exception $e){
            outLog('pdo_ping'.$e->getMessage());
        }
        restore_error_handler();
        return $conn;
    }
    public static function update($sql,$data,$param=array()) {
        try {
            $conn = self::getConn();
            $stmt = $conn->prepare($sql);
            $stmt->execute($data);
            if(!(isset($param['noLog']) && $param['noLog'] == true)){
                outLog($sql.' ['.json_encode($data).']');
            }
            if ($conn->errorCode() != '00000'){;
                outLog('update PDOException: ['.__FILE__.' '.__LINE__.' '.json_encode($stmt->errorInfo()).']');
                $stmt->debugDumpParams();
//                die();
            }

            return $stmt->rowCount();
        }catch(\PDOException $e)
        {
            outLog('update PDOException: ['.__FILE__.' '.__LINE__.' '.$e->getMessage().']');
            outLog('update failed ['.$sql.']{'.json_encode($data).'}');
            return -1;
        }
    }
    public static function query($sql){
        try{
            $conn = self::getConn();
            $result = $conn->query($sql);
            outLog($sql);
            if(is_array($result)){
                $rowList = [];
                foreach ($result as $row) {
                    $rowList[] = $row;
                }
                return $rowList;
            }
            return $result;
        }catch (\Exception $e){
            outLog($e->getMessage());
            return [];
        }
    }

    public static function lastInsertId()
    {
        $conn = self::getConn();
        return $conn->lastInsertId();
    }

    public static function select($sql,$fetch_style=\PDO::FETCH_ASSOC){
        try{
            $conn = self::getConn();
            $result = $conn->query($sql);
            if($result){
                $newArr = [];
                while($row = $result->fetch($fetch_style))
                {
                    $newArr[] = $row;
                }
                return $newArr;
            }else{
                return [];
            }
        }catch (\Exception $e){
            outLog($sql);
            outLog('Select Exception'.$e->getMessage());
            return [];
        }
    }

    public static function getOne($sql){
        $row = [];
        try{
            $conn = self::getConn();
            $result = $conn->query($sql);
            if(is_object($result)){
                $row = $result->fetch();
            }
        }catch (\Exception $e){
            outLog('getOne : ['.__FILE__.' '.__LINE__.' '.$e->getMessage().$sql.']');
        }
        if($row == false){
            $row = [];
        }
        return $row;
    }

    /**
     * 检查连接是否可用
     * @param Link $dbconn 数据库连接
     * @return Boolean
     */
    public static function pdo_ping($dbconn):bool
    {
        try {
            if(is_null($dbconn)){
                return false;
            }
            $dbconn->getAttribute(\PDO::ATTR_SERVER_INFO);
            return true;
        }catch (\PDOException $e) {
            if (strpos($e->getMessage(), 'MySQL server has gone away') !== false) {
                return false;
            }
        }catch (\Exception $e){
            outLog($e->getMessage());
        }
        return true;
    }

    public static function getConn(){
        global $conn;
        global $connTime;


        if(is_null($conn)){
            return self::getInstance();
        }

        if (!self::pdo_ping($conn) || (time() - $connTime) > 8600) {
            $connTime = time();
            return self::getInstance();
        }

        return $conn;
    }
    public  function close(){

    }
}

