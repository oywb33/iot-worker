<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/9/26 0026
 * Time: 18:22
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\system;


use app\action\Device;
use app\config\Constant;
use app\system\Factory\RedisFactory;
use tools\Tools;

class SyLog
{
    public $uid = '';
    public $log_path = '/data/logs';

    public function __construct(string $uid ='')
    {
        $this->uid = $uid;
    }

    public static function setUid($uid){
        $obj = new SyLog($uid);
        return $obj;
    }
    public static function createObj($uid = ''){
        $obj = new SyLog($uid);
        return $obj;
    }

    public static function print_log($uid,$msg){
        $echoKey = Constant::REDIS_SYSTEM_ECHO_LOG_PREFIX;
        if(empty($uid)){
            outLog($msg);
            return true;
        }
        if(empty($msg)){
            return false;
        }
        if(is_array($msg)){
            $msg = json_encode($msg);
        }
        $cachMsg = RedisFactory::getInstance()->hGet($echoKey,$uid);
        if($cachMsg == $msg){
            return false;
        }
        outLog($msg);
        RedisFactory::getInstance()->hMSet($echoKey, [$uid => $msg]);
        RedisFactory::getInstance()->expire($echoKey, Constant::REDIS_SYSTEM_LOG_TOUTIME);
        return true;
    }
    public function Log($msg)
    {
        try{
            if(is_array($msg)){
                $msg = Tools::json_encode($msg);
            }elseif (is_object($msg)){
                $msg = Tools::json_encode($msg);
            }elseif (is_bool($msg)){
                $msg = $msg ? 'true' : 'false';
            }else{
                $msg = (string)$msg;
            }
            if(empty($this->uid)){
                outLog($msg);
            }else{
                $msg = $this->uid.' | ' . $msg;
                $type_id = Device::getTypeByUid($this->uid);
                if($type_id === 0 or  $type_id === 4){
                    outLog($msg,$this->uid,Constant::FILE_OUT_BATTERY_NAME);
                }elseif ($type_id === 1){
                    outLog($msg,$this->uid,Constant::FILE_OUT_WASH_NAME);
                }elseif ($type_id === 5){
                    outLog($msg,$this->uid,Constant::FILE_OUT_CFJ_NAME);
                }else{
                    outLog($msg);
                }
            }
        }catch (\Exception $e){
            outLog($e->getMessage());
        }
    }

}