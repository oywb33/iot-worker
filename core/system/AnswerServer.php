<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/10 0010
 * Time: 16:28
 */

namespace app\system;

use app\config\SocketConfig;
use app\system\SocketPack;
use app\handler;
use PHPMailer\PHPMailer\Exception;

class AnswerServer
{
    protected $classname;
    protected $pack;
    protected $answer = '';
    protected $handler_answer = '';
    protected $end = false;

    public function __construct($pack)
    {
        $Communication_type = isset($pack->protocol_ename) ? $pack->protocol_ename : '';
        if($Communication_type == 'zhenyoukeji'){
//            outLog(' ===>'. $pack->action);
//            outLog($pack);
            $this->action_name = $pack->action;
            $this->pack = $pack;
            $this->protocol_type = 'zhenyoukeji';
        }else{
            $this->pack = $pack;
            $data = $pack->data;
            $action = isset($data['R']) ? $data['R'] : "";
            $this->action_name = SocketConfig::ACTION[$action];
        }
    }

    public function start()
    {
        $this->check();
        $this->run();
    }

    public function check()
    {
        if (empty($this->action_name)) {
            outLog('The action name of the packet does not exist');
        }
    }

    public function run()
    {
        /**
         * 这里需要重写
         */
        set_error_handler(function($errno, $errstr, $errfile, $errline, array $errcontext) {
            // error was suppressed with the @-operator
            if (0 === error_reporting()) {
                return false;
            }
            throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
        }, E_WARNING);
        try{
            $className = $this->Quote($this->action_name,$this->protocol_type);
            if(method_exists($className,'answer')){
                outLog('Quote | ' .$this->action_name);
                $this->answer = $className::answer($this->pack->data);
            }else{
                outLog('Quote Not Find ' .$this->action_name);
            }
        }catch (\Exception $e){
            outLog('AnswerServer '.$e->getMessage().PHP_EOL.$e->getTraceAsString());
        }
        restore_error_handler();
    }

    public function answer()
    {
        if (is_string($this->answer)) {
            return [$this->answer];
        }
        return $this->answer;
    }

    public $protocol_type = 'bj';
    public function Quote($className,$protocol_type='bj'){
        $obj = null;
        if($protocol_type == 'zhenyoukeji'){
            switch ($className) {
                case 'ReturnBack':
                    $obj = new handler\zhenyoukeji\ReturnBack();
                    break;
                case 'Heartbeat':
                    $obj = new handler\zhenyoukeji\Heartbeat();
                    break;
                case 'Lease':
                    break;
                case 'Login':
                    $obj = new handler\zhenyoukeji\Login();
                    break;
                case 'SyncSetting':
                    $obj = new handler\zhenyoukeji\SyncSetting();
                    break;
                case 'Repair':
                    break;
                case 'SyncBattery':
                    $obj = new handler\zhenyoukeji\SyncBattery();
                    break;
                case 'Debug':
                    $obj = new handler\zhenyoukeji\Debug();
                    break;
                case 'RentConfirm':
                    $obj = new handler\zhenyoukeji\RentConfirm();
                    break;
                case 'PopupConfirm':
                    $obj = new handler\zhenyoukeji\PopupConfirm();
                    break;
                case 'ShortHeartbeat':
                    break;
                case 'SpecialHeartbeat':
                    break;
                case 'Return':
                    break;
                default:
                    $className .= 'handler\zhenyoukeji\\'.$className;
                    outLog('not find '.$className. ' Handler');
//                    $obj = new $className();
                    break;
            }
        }
        else{
            switch ($className)
            {
                case 'ForceHeartbeat':
                    $obj = new handler\ForceHeartbeat();
                    break;
                case 'Heartbeat':
                    $obj = new handler\Heartbeat();
                    break;
                case 'Lease':
                    $obj = new handler\Lease();
                    break;
                case 'Register':
                    $obj = new handler\Register();
                    break;
                case 'Repair':
                    $obj = new handler\Repair();
                    break;
                case 'Restart':
                    $obj = new handler\Restart();
                    break;
                case 'Shutdown':
                    $obj = new handler\Shutdown();
                    break;
                case 'ShortHeartbeat':
                    $obj = new handler\ShortHeartbeat();
                    break;
                case 'SpecialHeartbeat':
                    $obj = new handler\SpecialHeartbeat();
                    break;
                case 'Return':
                    $obj = new handler\AppReturn();
                    break;
                default:
                    $obj = null;
                    outLog('not Handler');
                    break;
            }
        }

        return $obj;
    }

}
