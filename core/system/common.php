<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/10 0010
 * Time: 16:22
 */

/**
 * 把字符串转为32位符号整数
 */
function crc32hash($str)
{
    return sprintf('%u', crc32($str));
}

function getFileMimeType($filename)
{
    $finfo = finfo_open(FILEINFO_MIME);
    $mimetype = finfo_file($finfo, $filename);
    finfo_close($finfo);
    return $mimetype;
}

function get_extension($file)
{
    return pathinfo($file, PATHINFO_EXTENSION);
}

/*
 * 生成随机字符串
 * @param int $length 生成随机字符串的长度
 * @param string $char 组成随机字符串的字符串
 * @return string $string 生成的随机字符串
 */
function str_rand($length = 32, $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
{
    if (!is_int($length) || $length < 0) {
        return false;
    }

    $string = '';
    for ($i = $length; $i > 0; $i--) {
        $string .= $char[mt_rand(0, strlen($char) - 1)];
    }

    return $string;
}

/*
 * 生成32位唯一字符串
 */
function getUniqid()
{
    return md5(uniqid(microtime(true), true));
}


function getFileSize($size)
{
    $dw = "Byte";
    if ($size >= pow(2, 40)) {
        $size = round($size / pow(2, 40), 2);
        $dw = "TB";
    } else if ($size >= pow(2, 30)) {
        $size = round($size / pow(2, 30), 2);
        $dw = "GB";
    } else if ($size >= pow(2, 20)) {
        $size = round($size / pow(2, 20), 2);
        $dw = "MB";
    } else if ($size >= pow(2, 10)) {
        $size = round($size / pow(2, 10), 2);
        $dw = "KB";
    } else {
        $dw = "Bytes";
    }
    return $size . $dw;
}


function getRunSavePath()
{
    $save_path = \app\config\YaconfTools::getConfig('runpath');
    if (empty($save_path)) {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $save_path = "D:/tmp/var/";
        } else {
            $save_path = '/data/var/';
        }
    }
    if (!file_exists($save_path)) {
        mkdir($save_path);
        chmod($save_path, 0644);
    }
    return $save_path;
}


function outLog($msg, $out_log_Name = '', $logPath = '')
{
    try {
        if (empty($out_log_Name) or $out_log_Name == 'null') {
            $backtrace = \debug_backtrace();
            $_startFile = $backtrace[\count($backtrace) - 1]['file'];
            $out_log_Name = str_replace('.php', '', basename($_startFile));
        }

        if (is_array($msg)) {
            $msg = json_encode($msg);
        } elseif (is_object($msg)) {
            $msg = print_r($msg);
        }
        $msg = str_replace("\r\n", '\r\n', $msg);
        $save_path = \app\config\YaconfTools::getConfig('log_path');
        if (empty($save_path)) {
            $save_path = getRunSavePath();
        }

        $showtime = date("Ymd");
        if (!empty($logPath)) {
            $save_path .= $logPath;
            $logName = $out_log_Name . "_" . $showtime . ".log";
            $logFile = $save_path . $logName;
        } else {
            $logName = $out_log_Name . "_" . $showtime . ".log";
            $logFile = $save_path . $logName;
        }
//        $outMsg = $out_log_Name . ' | ' . date("Y-m-d H:i:s",time()) .' | ' . $msg . PHP_EOL;
        $outMsg = date("Y-m-d H:i:s", time()) . ' | ' . $msg . PHP_EOL;
        if (!file_exists($save_path)) {
            mkdir($save_path);
            chmod($save_path, 0644);
        }

//        echo 'LOG:'.$logFile.PHP_EOL;
        global $argv;
        $command2 = isset($argv[2]) ? \trim($argv[2]) : '';
        if ($command2 !== '-d') {
            echo $outMsg;
        }
        file_put_contents($logFile, $outMsg, FILE_APPEND | LOCK_EX);
    } catch (\Exception $e) {
        outLog($e->getMessage());
    }
}


function randomkeys($length)
{
    $key = '';
    $pattern = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLOMNOPQRSTUVWXYZ';
    for ($i = 0; $i < $length; $i++) {
        $key .= $pattern{mt_rand(0, 35)}; //生成php随机数
    }
    return $key;
}

function is_hex($hex_code)
{
    return @preg_match("/^[a-f0-9]{2,}$/i", $hex_code) && !(strlen($hex_code) & 1);
}

/**
 * 获得随机小数
 * @param int $min
 * @param int $max
 * @return float|int
 * USER: Administrator
 * TIME: 2020/3/6 0006 14:51
 */
function randFloat($min = 0, $max = 1)
{
    return $min + mt_rand() / mt_getrandmax() * ($max - $min);
}


/**
 * 将一个字符按比特位进行反转 eg: 65 (01000001) --> 130(10000010)
 * @param $char
 * @return $char
 */
function reverseChar($char)
{
    $byte = ord($char);
    $tmp = 0;
    for ($i = 0; $i < 8; ++$i) {
        if ($byte & (1 << $i)) {
            $tmp |= (1 << (7 - $i));
        }
    }
    return chr($tmp);
}

/**
 * 将一个字节流按比特位反转 eg: 'AB'(01000001 01000010)  --> '\x42\x82'(01000010 10000010)
 * @param $str
 */
function reverseString($str)
{
    $m = 0;
    $n = strlen($str) - 1;
    while ($m <= $n) {
        if ($m == $n) {
            $str{$m} = reverseChar($str{$m});
            break;
        }
        $ord1 = reverseChar($str{$m});
        $ord2 = reverseChar($str{$n});
        $str{$m} = $ord2;
        $str{$n} = $ord1;
        $m++;
        $n--;
    }
    return $str;
}


/**
 * PHP字符串“异或”算法
 */
function bcc($hexStr)
{
    $keyArr = str_split($hexStr, 2);
    $arrLength = count($keyArr);
    $initKey = "00000000000000000000000000000";
    $initKeyArr = str_split($initKey);
    for ($i = 0; $i < $arrLength; $i++) {
        $newKey = '';
        for ($j = 0; $j < strlen($keyArr[$i]); $j++) {
            $str = '';
            $tmpArr = str_split($keyArr[$i]);
            $tmpA = str_pad(base_convert($tmpArr[$j], 16, 2), 4, 0, STR_PAD_LEFT);
            $tmpB = str_pad(base_convert($initKeyArr[$j], 16, 2), 4, 0, STR_PAD_LEFT);
            for ($k = 0; $k < strlen($tmpA); $k++) {
                $str .= (intval($tmpA[$k]) ^ intval($tmpB[$k]));
            }
            $tmpOneKey = strtoupper(base_convert($str, 2, 16));
            unset($str);
            $newKey .= $tmpOneKey;
        }
        unset($initKeyArr);
        $initKeyArr = str_split($newKey);
    }
    return implode('', $initKeyArr);
}

/* 列举一些常用的crc16算法
 // CRC-16/IBM
printf("%x\n", crc16('1234567890', 0x8005, 0, 0, true, true));

// CRC-16/MAXIM
printf("%x\n", crc16('1234567890', 0x8005, 0, 0xffff, true, true));

// CRC-16/USB
printf("%x\n", crc16('1234567890', 0x8005, 0xffff, 0xffff, true, true));

// CRC-16/MODBUS
printf("%x\n", crc16('1234567890', 0x8005, 0xffff, 0, true, true));

// CRC-16/CCITT
printf("%x\n", crc16('1234567890', 0x1021, 0, 0, true, true));

// CRC-16/CCITT-FALSE
printf("%x\n", crc16('1234567890', 0x1021, 0xffff, 0, false, false));

// CRC-16/X25
printf("%x\n", crc16('1234567890', 0x1021, 0xffff, 0xffff, true, true));

// CRC-16/XMODEM
printf("%x\n", crc16('1234567890', 0x1021, 0, 0, false, false));

// CRC-16/DNP
printf("%x\n", crc16('1234567890', 0x3d65, 0, 0xffff, true, true));
*/
/**
 * @param string $str 待校验字符串
 * @param int $polynomial 二项式
 * @param int $initValue 初始值
 * @param int $xOrValue 输出结果前异或的值
 * @param bool $inputReverse 输入字符串是否每个字节按比特位反转
 * @param bool $outputReverse 输出是否整体按比特位反转
 * @return int
 */
function crc16($str, $polynomial, $initValue, $xOrValue, $inputReverse = false, $outputReverse = false)
{
    $crc = $initValue;

    for ($i = 0; $i < strlen($str); $i++) {
        if ($inputReverse) {
            // 输入数据每个字节按比特位逆转
            $c = ord(reverseChar($str{$i}));
        } else {
            $c = ord($str{$i});
        }
        $crc ^= ($c << 8);
        for ($j = 0; $j < 8; ++$j) {
            if ($crc & 0x8000) {
                $crc = (($crc << 1) & 0xffff) ^ $polynomial;
            } else {
                $crc = ($crc << 1) & 0xffff;
            }
        }
    }
    if ($outputReverse) {
        // 把低地址存低位，即采用小端法将整数转换为字符串
        $ret = pack('cc', $crc & 0xff, ($crc >> 8) & 0xff);
        // 输出结果按比特位逆转整个字符串
        $ret = reverseString($ret);
        // 再把结果按小端法重新转换成整数
        $arr = unpack('vshort', $ret);
        $crc = $arr['short'];
    }
    return $crc ^ $xOrValue;
}

/**
 * php实现的crc32函数
 * @param $str
 * @return int|mixed|string
 * USER: Administrator
 * TIME: 2020/3/19 0019 18:10
 */
function php_crc32($str)
{
    $polynomial = 0x04c11db7;
    $crc = 0xffffffff;
    for ($i = 0; $i < strlen($str); $i++) {
        $c = ord(reverseChar($str{$i}));
        $crc ^= ($c << 24);
        for ($j = 0; $j < 8; $j++) {
            if ($crc & 0x80000000) {
                $crc = (($crc << 1) & 0xffffffff) ^ $polynomial;
            } else {
                $crc = ($crc << 1) & 0xffffffff;
            }
        }
    }
    $ret = pack('cccc', $crc & 0xff, ($crc >> 8) & 0xff, ($crc >> 16) & 0xff, ($crc >> 24) & 0xff);
    $ret = reverseString($ret);
    $arr = unpack('Vret', $ret);
    $ret = $arr['ret'] ^ 0xffffffff;
    return $ret;
}

/**
 * 思迈尔专用 通讯CRC多项式：0x8408
 * 生成crc16校验位
 * @param array $data 16进制组成的数据如：[0x06,0x04,0x00,0x00,0xff,0x77]
 * @return string 返回iot设备定义校验规则的校验结果，16进制形式(去掉签名的0x)，如：3a9d
 */
function get_crc16(array $data)
{
    $h = 0xffff;
    foreach ($data as $value) {
        $unsigned_value = sprintf('%u', $value);
        $h ^= $unsigned_value;
        for ($i = 0; $i < 8; $i++) {
            $lsb = $h & 0x0001;
            $h >>= 1;
            if ($lsb == 1) {
                $h ^= 0x8408;
            }
        }
    }
    $h ^= 0xffff;
    return str_pad(dechex($h), 4, 0, STR_PAD_LEFT);
}

/**
 * 按字符串长度分割成数组,支持中文
 * @param $string
 * @param int $len
 * @return array
 */
function mbStrSplit($string, $len = 1)
{
    $start = 0;
    $strlen = mb_strlen($string);
    while ($strlen) {
        $array[] = mb_substr($string, $start, $len, "utf8");
        $string = mb_substr($string, $len, $strlen, "utf8");
        $strlen = mb_strlen($string);
    }
    return $array;
}

function get_msectime()
{
    list($msec, $sec) = explode(' ', microtime());
    $msectime = (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    return $msectime;
}

function getSysConfig($key = '')
{
    $cfg = include APP_PATH . 'config/config.php';
    if (!empty($key)) {
        return isset($cfg[$key]) ? $cfg[$key] : [];
    }
    return $cfg;
}

function exec_php_file($file)
{
    \ob_start();
    // Try to include php file.
    try {
        if (file_exists($file)) {
            include $file;
        }
    } catch (\Exception $e) {
        echo $e;
    }
    return \ob_get_clean();
}


//第一个是原串,第二个是 部份串
function startWith($str, $needle)
{

    return strpos($str, $needle) === 0;

}

//第一个是原串,第二个是 部份串
function endWith($haystack, $needle)
{

    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }
    return (substr($haystack, -$length) === $needle);
}


function delFile($path)
{
    $url = iconv('utf-8', 'gbk', $path);
    if (PATH_SEPARATOR == ':') { //linux
        if (file_exists($path)) unlink($path);
    } else {  //Windows
        if (file_exists($url)) unlink($url);
    }
}

/**
 * 将数据库中查出的列表以指定的 id 作为数组的键名
 * @param array $arr
 * @param string $key_name
 * @return array 处理后的数组
 */
function convert_arr_key($arr, $key_name)
{
    $arr2 = array();
    foreach ($arr as $key => $val) {
        $arr2[$val[$key_name]] = $val;
    }
    return $arr2;
}

/**
 * 十进制转二进制、八进制、十六进制 不足位数前面补零*
 * @param $datalist $datalist 传入数据array(100,123,130)
 * @param $bin  转换的进制可以是：2,8,16
 * @return array    返回数据 array() 返回没有数据转换的格式
 */
function decto_bin($datalist, $bin)
{
    /**
     * 测试：
     * var_dump(decto_bin(array(128,253),2));
     * var_dump(decto_bin(array(128,253),8));
     * var_dump(decto_bin(array(128,253),16));
     */
    static $arr = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F');
    $is_string = false;
    if (!is_array($datalist)) {
        $datalist = array($datalist);
        $is_string = true;
    }

    if ($bin == 10) return $datalist; //相同进制忽略
    $bytelen = ceil(16 / $bin); //获得如果是$bin进制，一个字节的长度
    $aOutChar = array();
    foreach ($datalist as $num) {
        $t = "";
        $num = intval($num);
        if ($num === 0) continue;
        while ($num > 0) {
            $t = $arr[$num % $bin] . $t;
            $num = floor($num / $bin);
        }
        $tlen = strlen($t);
        if ($tlen % $bytelen != 0) {
            $pad_len = $bytelen - $tlen % $bytelen;
            $t = str_pad("", $pad_len, "0", STR_PAD_LEFT) . $t; //不足一个字节长度，自动前面补充0
        }
        $aOutChar[] = $t;
    }
    if ($is_string && count($aOutChar) == 1) {
        return $aOutChar[0];
    }
    return $aOutChar;
}


function get_hash($chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()+-')
{
    $random = $chars[mt_rand(0, 73)] . $chars[mt_rand(0, 73)] . $chars[mt_rand(0, 73)] . $chars[mt_rand(0, 73)] . $chars[mt_rand(0, 73)];//Random 5 times
    $content = uniqid() . $random;  // 类似 5443e09c27bf4aB4uT
    return sha1($content);
}

function syserver_yaconf_get(string $key)
{
    return \Yaconf::get('syserver.base.' . $key);
}

function yaconf_get(string $key)
{
    return \Yaconf::get($key);
}


/**
 * 格式化code_sn
 * @param $codeSn
 * @param int $len
 * @return string
 */
function formatCodeSn($codeSn, $len = 7)
{
    $codeSn = trim((string)$codeSn);
    if (strlen($codeSn) != $len) {
        $codeSn = str_pad($codeSn, $len, "0", STR_PAD_LEFT);
    }
    return $codeSn;
}


function get_fsockopen($host, $port, $path, $cookie = '', $fget = false, $errno = null, $errstr = null)
{
    try {
        $fp = fsockopen($host, $port, $errno, $errstr, 30);
        if (!$fp) {
//            print "$errstr ($errno)<br />\n";
            return $errstr;
        }
        $out = "GET " . $path . " HTTP/1.1\r\n";
        $out .= "Host: " . $host . "\r\n";          //需要注意Host不能包括`http://`，仅可以使用`example.com`
        $out .= "Connection: Close\r\n";
        $out .= "Cookie: " . $cookie . "\r\n\r\n";

        fwrite($fp, $out);  //将请求写入socket

        if ($fget) {
            /**
             * 也可以选择获取server端的响应
             */
            while (!feof($fp)) {
                echo fgets($fp, 128);
            }
        }
//如果不等待server端响应直接关闭socket即可
        fclose($fp);
    } catch (\Exception $e) {
        echo $e->getMessage() . PHP_EOL . $e->getTraceAsString();
    }
}


/**
 * 将unicode字符串按传入长度分割成数组
 * @param  string $str 传入字符串
 * @param  integer $l 字符串长度
 * @return mixed      数组或false
 */
function str_split_unicode($str, $l = 0)
{
    if ($l > 0) {
        $ret = array();
        $len = mb_strlen($str, "UTF-8");
        for ($i = 0; $i < $len; $i += $l) {
            $ret[] = mb_substr($str, $i, $l, "UTF-8");
        }
        return $ret;
    }
    return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
}

function identically_equal($a,$b){
    if($b === $a){
        return true;
    }
    return false;
}

//function BytesToStr($bytes)
//{
//    $str = '';
//    foreach ($bytes as $ch) {
//        $str .= chr($ch);
//    }
//    return $str;
//}