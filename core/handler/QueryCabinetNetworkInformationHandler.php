<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/12 0012
 * Time: 10:18
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\handler\shark;

use core\handler\Handler;

abstract class QueryCabinetNetworkInformationHandler  extends Handler
{

    public function answer(){
        $this->send_msg = '';
        return $this->send_msg;
    }


    public function onSend()
    {

        $this->send_msg = '';
        $this->send_msg = $this->packet->outPack($this->send_msg);
        return $this->send_msg;
    }

}