<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/12 0012
 * Time: 10:17
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace core\handler;


use core\handler\Handler;

abstract class RemoteRestartCabinetHandler  extends Handler
{

    public function answer(){
        $this->send_msg = '';
        return $this->send_msg;
    }

    public function onSend()
    {
        $this->send_msg = '';
        $this->send_msg = $this->packet->outPack($this->send_msg);
        return $this->send_msg;
    }
}