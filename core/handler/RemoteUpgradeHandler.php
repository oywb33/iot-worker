<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/12 0012
 * Time: 10:17
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace core\handler;



use core\handler\Handler;

abstract class RemoteUpgradeHandler  extends Handler
{

    private $FTPAddress;
    private $FTPPort;
    private $FileName;
    private $Username;
    private $Password;

    public function answer(){
        $this->send_msg = '';
        return $this->send_msg;
    }

    public function onSend()
    {

        $this->send_msg = $this->packet->outPack($this->send_msg);
        return $this->send_msg;
    }

    /**
     * @return mixed
     */
    public function getFTPAddress()
    {
        return $this->FTPAddress;
    }

    /**
     * @param mixed $FTPAddress
     */
    public function setFTPAddress($FTPAddress): void
    {
        $this->FTPAddress = $FTPAddress;
    }

    /**
     * @return mixed
     */
    public function getFTPPort()
    {
        return $this->FTPPort;
    }

    /**
     * @param mixed $FTPPort
     */
    public function setFTPPort($FTPPort): void
    {
        $this->FTPPort = $FTPPort;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->FileName;
    }

    /**
     * @param mixed $FileName
     */
    public function setFileName($FileName): void
    {
        $this->FileName = $FileName;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * @param mixed $Username
     */
    public function setUsername($Username): void
    {
        $this->Username = $Username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @param mixed $Password
     */
    public function setPassword($Password): void
    {
        $this->Password = $Password;
    }



}