<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/12 0012
 * Time: 10:16
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace core\handler;


use core\handler\Handler;

abstract class HeartbeatHandler extends  Handler
{

    /**
     * 心跳
     */

    public function answer(){
        return $this->send_msg;
    }

    public function onSend()
    {
        $this->send_msg = '';
        return $this->send_msg;
    }

}