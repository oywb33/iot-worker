<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/12 0012
 * Time: 10:17
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\handler\shark;

use app\dao\BatteryDao;
use app\dao\Packet;
use app\dao\TaskDao;
use app\system\SyLog;

use core\handler\Handler;

abstract class ReturnPowerBankHandler  extends Handler
{
    public function answer(){

        return $this->send_msg;
    }
    public function onSend()
    {
        $this->send_msg = '';
        return $this->send_msg;
    }

    public function after(){
        $this-> QueryStock();
        return null;
    }

}