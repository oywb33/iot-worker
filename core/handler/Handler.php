<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/3/10 0010
 * Time: 16:32
 */

namespace core\handler;


use app\config\Constant;
use app\dao\Packet;

abstract class Handler
{
    public $client_id = '';
    public $uid = '';
    public $message = '';

    /**
     * @var Packet|null
     */
    public $packet = null;
    public $send_msg = '';
    public $up_pack = null;
    public $down_pack = null;
    /**
     * @var Task|null
     */
    public $task = null;
    public $created = 0;
    public $out_time = 0;
    public $after = '';
    public $before = '';

    public $_now_time = 0;


    /**
     * Handler constructor.
     * @param Packet|null $packet
     * @param Task|null $task
     */
    public function __construct( Packet $packet = null, Task $task = null)
    {

        $this->packet = $packet;
        $this->client_id = $this->packet->getClientId();
        $this->task = $task;
        $this->out_time = $this->created + Constant::REDIS_TOUTIME;
        $this->_now_time = time();
        $this->created = $this->_now_time;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getNowTime(): int
    {
        return $this->_now_time;
    }


    public function answer(){

    }


    public function onReceive($packet=null)
    {
        if(empty($packet)){
            $this->packet;
        }else{
            $this->packet = $packet;
        }
    }

    public function onAction(){
        return 'client_id:'.$this->client_id.' onAction:['.$this->message.']';
    }

    public function onSend()
    {
        return '';
    }

    public function after()
    {
        return true;
    }
    public function before(){
        return true;
    }


    public function QueryStock(){

    }

    public function QueryCabinetVoiceBroadcastVolume(){

    }

    public function QueryCabinetNetworkInformation(){

    }

    public function QueryServerAddress(){

    }

    public function QueryCabinetSoftware(){

    }


    public function QueryCCID(){

    }
}