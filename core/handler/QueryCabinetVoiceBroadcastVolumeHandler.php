<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/12 0012
 * Time: 10:18
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace core\handler;



abstract class QueryCabinetVoiceBroadcastVolumeHandler  extends Handler
{

    public function answer(){
        $this->send_msg = '';
        return $this->send_msg;
    }
    public function onSend()
    {

        $this->send_msg = '';
        if($this->packet->haveValue($this->packet->getSlot())){
            $Slot = str_pad(dechex($this->packet->getSlot()), 2, 0, STR_PAD_LEFT);
            $this->send_msg .= $Slot;
        }
        $this->send_msg = $this->packet->outPack($this->send_msg);
        return $this->send_msg;
    }
}