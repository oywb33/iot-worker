<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/12 0012
 * Time: 10:15
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace core\handler;


abstract class LoginHandler  extends Handler
{

    /**
     * 登陆及响应
     */


    public function answer(){

        return $this->send_msg;
    }

    public function after()
    {

    }

    public function onSend()
    {
        $this->send_msg = '';
        return $this->send_msg;
    }


}