<?php
/**
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/27 0027
 * Time: 16:44
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\exception;


class BatteryNotchException extends \Exception
{
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}