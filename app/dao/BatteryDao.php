<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/27 0027
 * Time: 16:20
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\dao;

use app\config\BatteryConfig;
use app\exception\BatteryNotchException;
use app\exception\ErrorCode;

class BatteryDao
{
    public $id = 0;
    public $type_id = 0;
    public $device_id = '';
    public $B1 = '';
    public $B2 = 0;
    public $B3 = 0;
    public $B4 = 0;
    public $B5 = 0;
    public $color_id = '';
    public $voltage = '';
    public $ampere = '';
    public $temperature = '';
    public $battery_type = '';
    public $used_num = 0;
    public $status = 0;
    public $lock = 0;
    public $updated = 0;
    public $created = 0;
    public $sort_id = 0;
    public $fail_num = 0;
    public $last_fail_detail = '';
    public $__table_name = 'cdy_battery';

    /**
     * BatteryDao constructor.
     * @param int $type_id
     * @param string $device_id
     * @param string $B1
     * @param int $B2
     */
    public function __construct(int $type_id, string $device_id, string $B1, int $B2=0)
    {
        $this->type_id = $type_id;
        $this->device_id = $device_id;
        $this->B1 = $B1;
        $this->B2 = $B2;
        $this->B3 = 40;
        $this->created = time();
        $this->updated = time();
        $this->used_num = 0;

        if (empty($this->B1) or $this->B1 == '0000000000' or preg_match("/^[0]*$/", $this->B1)) {
            $status = BatteryConfig::STATUS_ERROR;
            $batteryLock = BatteryConfig::BATTERY_LOCK_YES;
        } elseif ($this->B4 > 0 or $this->B5 > 0) {
            $status = BatteryConfig::STATUS_ERROR;
            $batteryLock = BatteryConfig::BATTERY_LOCK_YES;
        } else {
            $status = BatteryConfig::STATUS_IN;
            $batteryLock = BatteryConfig::BATTERY_LOCK_NO;
        }
        $this->status = $status;
        $this->lock = $batteryLock;

        if($this->B2 < 0 or $this->B2 > 100){
            new BatteryNotchException('错误的槽口',ErrorCode::ERROR_BATTERY_NOTCH);
        }
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->type_id;
    }

    /**
     * @param int $type_id
     */
    public function setTypeId(int $type_id): void
    {
        $this->type_id = $type_id;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->device_id;
    }

    /**
     * @param string $device_id
     */
    public function setDeviceId(string $device_id): void
    {
        $this->device_id = $device_id;
    }


    /**
     * @return string
     */
    public function getColorId(): string
    {
        return $this->color_id;
    }

    /**
     * @param string $color_id
     */
    public function setColorId(string $color_id): void
    {
        $this->color_id = $color_id;
    }

    /**
     * @return string
     */
    public function getVoltage(): string
    {
        return $this->voltage;
    }

    /**
     * @param string $voltage
     */
    public function setVoltage(string $voltage): void
    {
        $this->voltage = $voltage;
    }

    /**
     * @return string
     */
    public function getAmpere(): string
    {
        return $this->ampere;
    }

    /**
     * @param string $ampere
     */
    public function setAmpere(string $ampere): void
    {
        $this->ampere = $ampere;
    }

    /**
     * @return string
     */
    public function getTemperature(): string
    {
        return $this->temperature;
    }

    /**
     * @param string $temperature
     */
    public function setTemperature(string $temperature): void
    {
        $this->temperature = $temperature;
    }

    /**
     * @return string
     */
    public function getBatteryType(): string
    {
        return $this->battery_type;
    }

    /**
     * @param string $battery_type
     */
    public function setBatteryType(string $battery_type): void
    {
        $this->battery_type = $battery_type;
    }

    /**
     * @return int
     */
    public function getUsedNum(): int
    {
        return $this->used_num;
    }

    /**
     * @param int $used_num
     */
    public function setUsedNum(int $used_num): void
    {
        $this->used_num = $used_num;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getLock(): int
    {
        return $this->lock;
    }

    /**
     * @param int $lock
     */
    public function setLock(int $lock): void
    {
        $this->lock = $lock;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getCreated(): int
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated(int $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getSortId(): int
    {
        return $this->sort_id;
    }

    /**
     * @param int $sort_id
     */
    public function setSortId(int $sort_id): void
    {
        $this->sort_id = $sort_id;
    }

    /**
     * @return int
     */
    public function getFailNum(): int
    {
        return $this->fail_num;
    }

    /**
     * @param int $fail_num
     */
    public function setFailNum(int $fail_num): void
    {
        $this->fail_num = $fail_num;
    }

    /**
     * @return string
     */
    public function getLastFailDetail(): string
    {
        return $this->last_fail_detail;
    }

    /**
     * @param string $last_fail_detail
     */
    public function setLastFailDetail(string $last_fail_detail): void
    {
        $this->last_fail_detail = $last_fail_detail;
    }

}