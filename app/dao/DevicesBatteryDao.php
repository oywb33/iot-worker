<?php
/**
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/27 0027
 * Time: 16:24
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\dao;


use app\exception\BatteryNotchException;
use app\exception\ErrorCode;

class DevicesBatteryDao
{
    public $id = 0;
    public $type_id = 0;
    public $device_id = '';
    public $battery_id = '';
    public $site = 0;
    public $used_num = 0;
    public $status = 0;
    public $updated = 0;
    public $created = 0;
    public $__table_name = 'cdy_devices_battery';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->type_id;
    }

    /**
     * @param int $type_id
     */
    public function setTypeId(int $type_id): void
    {
        $this->type_id = $type_id;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->device_id;
    }

    /**
     * @param string $device_id
     */
    public function setDeviceId(string $device_id): void
    {
        $this->device_id = $device_id;
    }

    /**
     * @return string
     */
    public function getBatteryId(): string
    {
        return $this->battery_id;
    }

    /**
     * @param string $battery_id
     */
    public function setBatteryId(string $battery_id): void
    {
        $this->battery_id = $battery_id;
    }

    /**
     * @return int
     */
    public function getSite(): int
    {
        return $this->site;
    }

    /**
     * @param int $site
     */
    public function setSite(int $site): void
    {
        $this->site = $site;
    }

    /**
     * @return int
     */
    public function getUsedNum(): int
    {
        return $this->used_num;
    }

    /**
     * @param int $used_num
     */
    public function setUsedNum(int $used_num): void
    {
        $this->used_num = $used_num;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getCreated(): int
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated(int $created): void
    {
        $this->created = $created;
    }


    /**
     * DevicesBatteryDao constructor.
     * @param int $type_id
     * @param string $device_id
     * @param string $battery_id
     * @param int $site
     */
    public function __construct(int $type_id, string $device_id, string $battery_id, int $site)
    {
        $this->type_id = $type_id;
        $this->device_id = $device_id;
        $this->battery_id = $battery_id;
        $this->site = $site;
        $this->updated = time();
        $this->created = time();
        if($this->$site < 0 or $this->$site > 50){
            new BatteryNotchException('错误的槽口',ErrorCode::ERROR_BATTERY_NOTCH);
        }
    }

}