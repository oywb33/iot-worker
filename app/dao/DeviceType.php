<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/27 0027
 * Time: 16:19
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\dao;


class DeviceType
{
    public $id = 0;
    public $prefix = '';
    public $factory_id = 0;
    public $type_remask = '';
    public $__table_name = 'cdy_device_type';

    /**
     * DeviceType constructor.
     * @param int $id
     * @param string $prefix
     * @param int $factory_id
     */
    public function __construct(int $id, string $prefix='', int $factory_id=0)
    {
        $this->id = $id;
        $this->prefix = $prefix;
        $this->factory_id = $factory_id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     */
    public function setPrefix(string $prefix): void
    {
        $this->prefix = $prefix;
    }

    /**
     * @return int
     */
    public function getFactoryId(): int
    {
        return $this->factory_id;
    }

    /**
     * @param int $factory_id
     */
    public function setFactoryId(int $factory_id): void
    {
        $this->factory_id = $factory_id;
    }

    /**
     * @return string
     */
    public function getTypeRemask(): string
    {
        return $this->type_remask;
    }

    /**
     * @param string $type_remask
     */
    public function setTypeRemask(string $type_remask): void
    {
        $this->type_remask = $type_remask;
    }

}