<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/27 0027
 * Time: 16:14
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\dao;


class TaskDao
{
    public $id = 0;
    public $site_id = 0;
    public $type_id = 0;
    public $order_id = '';
    public $code_sn = '';
    public $task_name = '';
    public $task_type = '';
    public $device_id = '';
    public $battery_b1 = '';
    public $bayonet = 0;
    public $created = 0;
    public $updated = 0;
    public $status = 0;
    public $re_task_id = 0;
    public $env = '';
    public $check_status = '';
    public $api_type = '';
    public $parameter = '';
    public $remark = '';
    public $message_uri = '';
    public $return_uri = '';
    public $expect_time = '';
    public $client_id = 0;
    public $__table_name = 'cdy_tasks';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSiteId(): int
    {
        return $this->site_id;
    }

    /**
     * @param int $site_id
     */
    public function setSiteId(int $site_id): void
    {
        $this->site_id = $site_id;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->type_id;
    }

    /**
     * @param int $type_id
     */
    public function setTypeId(int $type_id): void
    {
        $this->type_id = $type_id;
    }

    /**
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->order_id;
    }

    /**
     * @param string $order_id
     */
    public function setOrderId(string $order_id): void
    {
        $this->order_id = $order_id;
    }

    /**
     * @return string
     */
    public function getCodeSn(): string
    {
        return $this->code_sn;
    }

    /**
     * @param string $code_sn
     */
    public function setCodeSn(string $code_sn): void
    {
        $this->code_sn = $code_sn;
    }

    /**
     * @return string
     */
    public function getTaskName(): string
    {
        return $this->task_name;
    }

    /**
     * @param string $task_name
     */
    public function setTaskName(string $task_name): void
    {
        $this->task_name = $task_name;
    }

    /**
     * @return string
     */
    public function getTaskType(): string
    {
        return $this->task_type;
    }

    /**
     * @param string $task_type
     */
    public function setTaskType(string $task_type): void
    {
        $this->task_type = $task_type;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return (string)$this->device_id;
    }

    /**
     * @param string $device_id
     */
    public function setDeviceId(string $device_id): void
    {
        $this->device_id = $device_id;
    }

    /**
     * @return string
     */
    public function getBatteryB1(): string
    {
        return (string)$this->battery_b1;
    }

    /**
     * @param string $battery_b1
     */
    public function setBatteryB1(string $battery_b1): void
    {
        $this->battery_b1 = $battery_b1;
    }

    /**
     * @return int
     */
    public function getBayonet(): int
    {
        return (int)$this->bayonet;
    }

    /**
     * @param int $bayonet
     */
    public function setBayonet(int $bayonet): void
    {
        $this->bayonet = $bayonet;
    }

    /**
     * @return int
     */
    public function getCreated(): int
    {
        return (int)$this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated(int $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return (int)$this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return (int)$this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getReTaskId(): int
    {
        return (int)$this->re_task_id;
    }

    /**
     * @param int $re_task_id
     */
    public function setReTaskId(int $re_task_id): void
    {
        $this->re_task_id = $re_task_id;
    }

    /**
     * @return string
     */
    public function getEnv(): string
    {
        return $this->env;
    }

    /**
     * @param string $env
     */
    public function setEnv(string $env): void
    {
        $this->env = $env;
    }

    /**
     * @return string
     */
    public function getCheckStatus(): string
    {
        return $this->check_status;
    }

    /**
     * @param string $check_status
     */
    public function setCheckStatus(string $check_status): void
    {
        $this->check_status = $check_status;
    }

    /**
     * @return string
     */
    public function getApiType(): string
    {
        return $this->api_type;
    }

    /**
     * @param string $api_type
     */
    public function setApiType(string $api_type): void
    {
        $this->api_type = $api_type;
    }

    /**
     * @return string
     */
    public function getParameter(): string
    {
        return $this->parameter;
    }

    /**
     * @param string $parameter
     */
    public function setParameter(string $parameter): void
    {
        $this->parameter = $parameter;
    }

    /**
     * @return string
     */
    public function getRemark(): string
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark(string $remark): void
    {
        $this->remark = $remark;
    }

    /**
     * @return string
     */
    public function getMessageUri(): string
    {
        return $this->message_uri;
    }

    /**
     * @param string $message_uri
     */
    public function setMessageUri(string $message_uri): void
    {
        $this->message_uri = $message_uri;
    }

    /**
     * @return string
     */
    public function getReturnUri(): string
    {
        return $this->return_uri;
    }

    /**
     * @param string $return_uri
     */
    public function setReturnUri(string $return_uri): void
    {
        $this->return_uri = $return_uri;
    }

    /**
     * @return string
     */
    public function getExpectTime(): string
    {
        return $this->expect_time;
    }

    /**
     * @param string $expect_time
     */
    public function setExpectTime(string $expect_time): void
    {
        $this->expect_time = $expect_time;
    }

    /**
     * @return int
     */
    public function getClientId(): int
    {
        return $this->client_id;
    }

    /**
     * @param int $client_id
     */
    public function setClientId(int $client_id): void
    {
        $this->client_id = $client_id;
    }


    /**
     * TaskDao constructor.
     * @param int $site_id
     * @param int $type_id
     * @param string $order_id
     * @param string $code_sn
     * @param string $client_id
     * @param string $task_name
     * @param string $task_type
     * @param string $device_id
     * @param string $battery_b1
     * @param int $bayonet
     * @param int $created
     * @param int $updated
     * @param int $status
     * @param int $re_task_id
     * @param string $api_type
     */
    public function __construct(string $task_type, string $device_id, string $code_sn,string $client_id, int $status=0, string $task_name = '', string $battery_b1 = '', int $bayonet = 0, string $order_id = '', int $created = 0, int $updated = 0, int $re_task_id = 0, string $api_type = 'v1', int $site_id = 0, int $type_id = 0)
    {
        $this->site_id = $site_id;
        $this->type_id = $type_id;
        $this->order_id = $order_id;
        $this->code_sn = $code_sn;
        $this->task_name = $task_name;
        $this->task_type = $task_type;
        $this->device_id = $device_id;
        $this->battery_b1 = $battery_b1;
        $this->bayonet = $bayonet;
        $this->created = $created;
        $this->updated = $updated;
        $this->status = $status;
        $this->re_task_id = $re_task_id;
        $this->api_type = $api_type;
        $this->client_id = $client_id;

        if(empty($this->created)){
            $this->created = time();
        }
        if(empty($this->updated)){
            $this->updated = time();
        }
        if(empty($this->status)){
            $this->status = 0;
        }
        if(empty($this->api_type)){
            $this->api_type = 'v1';
        }
        if(empty($this->task_name)){
            $this->task_name = $this->task_type.'_'.$this->created.'_'.$this->status.'_'.$this->site_id.'_'.$this->api_type;
        }
    }

}