<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/3/11 0011
 * Time: 17:48
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\dao;

use app\config\BatteryConfig;
use app\config\Constant;

class Packet
{
    /**
     * 包的对应客户端id
     * @var string
     */
    public $client_id = '';

    /**
     * 绑定的设备ID
     * @var null
     */
    public $uid = null;
    /**
     * PacketLen 数据包的长度 Uint16 2
     * @var int
     */
    public $packetLen = 2;
    //Command 命令字 Byte 1
    public $command = 1;
    /**
     * 协议版本号 Byte 1
     * @var int
     */
    public $vsn = 01;

    /**
     * 有效数据与否 有效数据（ Payload） 的字节异或 Byte 1
     * @var int|string
     */
    public $checkSum = '';

    /**
     * 会话令牌 Uint32 4
     * @var int
     */
    public $token = 11223344;
    //Payload 有效数据，不定长 Byte
    public $payLoad = 0;
    //注意：所有的字符串数据内容都需要以 0x00 结束，字符串的长度包含 0x00。CheckSum 字段由Payload 有效数据计算得出
    public $headLength = 9;

    public $data = null;

    //随机数 Uint32 4
    public $rand = null;
    //魔术字 Uint16 2
    public $magic = null;

    //BoxID 的长度 Uint16 2
    public $BoxIDLen = -1;
    //柜机SN和柜身的二维码对应 String BoxIDLen
    public $BoxID = null;
    public $device_type_id = 0;

    //机柜软件版本号数据长度
    public $SoftVerLen = -1;
    //机柜软件版本号
    public $SoftVer = null;
    //服务器地址长度
    public $AddressLen = -1;
    //服务器地址
    public $Address = null;
    //服务器端口长度
    public $PortLen = -1;
    //服务器端口
    public $Port = -1;
    //心跳间隔(1~255有效)
    public $Heartbeat = -1;

    //剩余充电宝个数
    public $RemainNum = -1;

    public $batteryList = [];

    public $battery_type_id = 0;
    //槽位编号 Byte 1
    public $Slot = -1;
    //充电宝ID Byte 8
    public $TerminalID = null;
    //充电宝电量 Byte 1
    public $Level = -1;

    //借用结果： 0：失败 1：成功
    public $Result = -1;

    //FTP 服务器地址长度
    public $FTPAddressLen = -1;
    //FTP 服务器地址
    public $FTPAddress = null;
    //FTP 服务器端口长度
    public $FTPPortLen = -1;
    //FTP 服务器端口
    public $FTPPort = null;
    //文件名长度
    public $FileNameLen = -1;
    //文件名
    public $FileName = null;
    //用户名长度
    public $UsernameLen = -1;
    //用户名
    public $Username = null;
    //密码长度
    public $PasswordLen = -1;
    //密码
    public $Password = null;

    //ICCID 的长度
    public $ICCIDLen = -1;
    //SIM 卡的ICCID
    public $ICCID = null;

    //音量大小(0 到 15) Byte 1
    public $Lvl = -1;
    //信号强度（0 到 31）
    public $CSQ = -1;
    //误码率
    public $SER = -1;
    //网络制式：  2：GSM/GPRS/EDGE 网络 3：WCDMA 网络 7：LTE 网络 5: WI-FI  Byte 1
    public $Modev = -1;
    public $Inet = null;

    /**
     * @param int $Lvl
     */
    public function setLvl(int $Lvl): void
    {
        if($Lvl > 15){
            echo '错误的音量参数'.PHP_EOL;
            $Lvl = 15;
        }
        $this->Lvl = $Lvl;
    }

    /**
     * Packet constructor.
     * @param int $packetLen
     * @param int $command
     * @param int $vsn
     * @param int $checkSum
     * @param int $token
     * @param int $payLoad
     */
    public function __construct($client_id,$packetLen=0, $command=0, $vsn=1, $checkSum=0, $token=11223344, $payLoad=0)
    {
        $this->client_id = $client_id;
        $this->packetLen = $packetLen;
        $this->command = $command;
        $this->vsn = $vsn;
        $this->checkSum = $checkSum;
        $this->token = $token;
        $this->payLoad = $payLoad;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->client_id;
    }

    /**
     * @param string $client_id
     */
    public function setClientId(string $client_id): void
    {
        $this->client_id = $client_id;
    }

    /**
     * @return null
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param null $uid
     */
    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getPacketLen(): int
    {
        return $this->packetLen;
    }

    /**
     * @param int $packetLen
     */
    public function setPacketLen(int $packetLen): void
    {
        $this->packetLen = $packetLen;
    }

    /**
     * @return int
     */
    public function getCommand(): int
    {
        return $this->command;
    }

    /**
     * @param int $command
     */
    public function setCommand(int $command): void
    {
        $this->command = $command;
    }

    /**
     * @return int
     */
    public function getVsn(): int
    {
        return $this->vsn;
    }

    /**
     * @param int $vsn
     */
    public function setVsn(int $vsn): void
    {
        $this->vsn = $vsn;
    }

    /**
     * @return int|string
     */
    public function getCheckSum()
    {
        return $this->checkSum;
    }

    /**
     * @param int|string $checkSum
     */
    public function setCheckSum($checkSum): void
    {
        $this->checkSum = $checkSum;
    }

    /**
     * @return int
     */
    public function getToken(): int
    {
        return $this->token;
    }

    /**
     * @param int $token
     */
    public function setToken(int $token): void
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getPayLoad(): int
    {
        return $this->payLoad;
    }

    /**
     * @param int $payLoad
     */
    public function setPayLoad(int $payLoad): void
    {
        $this->payLoad = $payLoad;
    }

    /**
     * @return int
     */
    public function getHeadLength(): int
    {
        return $this->headLength;
    }

    /**
     * @param int $headLength
     */
    public function setHeadLength(int $headLength): void
    {
        $this->headLength = $headLength;
    }

    /**
     * @return null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param null $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

    /**
     * @return null
     */
    public function getRand()
    {
        return $this->rand;
    }

    /**
     * @param null $rand
     */
    public function setRand($rand): void
    {
        $this->rand = $rand;
    }

    /**
     * @return null
     */
    public function getMagic()
    {
        return $this->magic;
    }

    /**
     * @param null $magic
     */
    public function setMagic($magic): void
    {
        $this->magic = $magic;
    }

    /**
     * @return int
     */
    public function getBoxIDLen(): int
    {
        return $this->BoxIDLen;
    }

    /**
     * @param int $BoxIDLen
     */
    public function setBoxIDLen(int $BoxIDLen): void
    {
        $this->BoxIDLen = $BoxIDLen;
    }

    /**
     * @return null
     */
    public function getBoxID()
    {
        return $this->BoxID;
    }

    /**
     * @param null $BoxID
     */
    public function setBoxID($BoxID): void
    {
        $this->BoxID = $BoxID;
    }

    /**
     * @return int
     */
    public function getDeviceTypeId(): int
    {
        return $this->device_type_id;
    }

    /**
     * @param int $device_type_id
     */
    public function setDeviceTypeId(int $device_type_id): void
    {
        $this->device_type_id = $device_type_id;
    }

    /**
     * @return int
     */
    public function getSoftVerLen(): int
    {
        return $this->SoftVerLen;
    }

    /**
     * @param int $SoftVerLen
     */
    public function setSoftVerLen(int $SoftVerLen): void
    {
        $this->SoftVerLen = $SoftVerLen;
    }

    /**
     * @return null
     */
    public function getSoftVer()
    {
        return $this->SoftVer;
    }

    /**
     * @param null $SoftVer
     */
    public function setSoftVer($SoftVer): void
    {
        $this->SoftVer = $SoftVer;
    }

    /**
     * @return int
     */
    public function getAddressLen(): int
    {
        return $this->AddressLen;
    }

    /**
     * @param int $AddressLen
     */
    public function setAddressLen(int $AddressLen): void
    {
        $this->AddressLen = $AddressLen;
    }

    /**
     * @return null
     */
    public function getAddress()
    {
        return $this->Address;
    }

    /**
     * @param null $Address
     */
    public function setAddress($Address): void
    {
        $this->Address = $Address;
    }

    /**
     * @return int
     */
    public function getPortLen(): int
    {
        return $this->PortLen;
    }

    /**
     * @param int $PortLen
     */
    public function setPortLen(int $PortLen): void
    {
        $this->PortLen = $PortLen;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->Port;
    }

    /**
     * @param int $Port
     */
    public function setPort(int $Port): void
    {
        $this->Port = $Port;
    }

    /**
     * @return int
     */
    public function getHeartbeat(): int
    {
        return $this->Heartbeat;
    }

    /**
     * @param int $Heartbeat
     */
    public function setHeartbeat(int $Heartbeat): void
    {
        $this->Heartbeat = $Heartbeat;
    }

    /**
     * @return int
     */
    public function getRemainNum(): int
    {
        return $this->RemainNum;
    }

    /**
     * @param int $RemainNum
     */
    public function setRemainNum(int $RemainNum): void
    {
        $this->RemainNum = $RemainNum;
    }

    /**
     * @return array
     */
    public function getBatteryList(): array
    {
        return $this->batteryList;
    }

    /**
     * @param array $batteryList
     */
    public function setBatteryList(array $batteryList): void
    {
        $this->batteryList = $batteryList;
    }

    /**
     * @return int
     */
    public function getBatteryTypeId(): int
    {
        return $this->battery_type_id;
    }

    /**
     * @param int $battery_type_id
     */
    public function setBatteryTypeId(int $battery_type_id): void
    {
        $this->battery_type_id = $battery_type_id;
    }

    /**
     * @return int
     */
    public function getSlot(): int
    {
        return $this->Slot;
    }

    /**
     * @param int $Slot
     */
    public function setSlot(int $Slot): void
    {
        $this->Slot = $Slot;
    }

    /**
     * @return null
     */
    public function getTerminalID()
    {
        return $this->TerminalID;
    }

    /**
     * @param null $TerminalID
     */
    public function setTerminalID($TerminalID): void
    {
        $this->TerminalID = $TerminalID;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->Level;
    }

    /**
     * @param int $Level
     */
    public function setLevel(int $Level): void
    {
        $this->Level = $Level;
    }

    /**
     * @return int
     */
    public function getResult(): int
    {
        return $this->Result;
    }

    /**
     * @param int $Result
     */
    public function setResult(int $Result): void
    {
        $this->Result = $Result;
    }

    /**
     * @return int
     */
    public function getFTPAddressLen(): int
    {
        return $this->FTPAddressLen;
    }

    /**
     * @param int $FTPAddressLen
     */
    public function setFTPAddressLen(int $FTPAddressLen): void
    {
        $this->FTPAddressLen = $FTPAddressLen;
    }

    /**
     * @return null
     */
    public function getFTPAddress()
    {
        return $this->FTPAddress;
    }

    /**
     * @param null $FTPAddress
     */
    public function setFTPAddress($FTPAddress): void
    {
        $this->FTPAddress = $FTPAddress;
    }

    /**
     * @return int
     */
    public function getFTPPortLen(): int
    {
        return $this->FTPPortLen;
    }

    /**
     * @param int $FTPPortLen
     */
    public function setFTPPortLen(int $FTPPortLen): void
    {
        $this->FTPPortLen = $FTPPortLen;
    }

    /**
     * @return null
     */
    public function getFTPPort()
    {
        return $this->FTPPort;
    }

    /**
     * @param null $FTPPort
     */
    public function setFTPPort($FTPPort): void
    {
        $this->FTPPort = $FTPPort;
    }

    /**
     * @return int
     */
    public function getFileNameLen(): int
    {
        return $this->FileNameLen;
    }

    /**
     * @param int $FileNameLen
     */
    public function setFileNameLen(int $FileNameLen): void
    {
        $this->FileNameLen = $FileNameLen;
    }

    /**
     * @return null
     */
    public function getFileName()
    {
        return $this->FileName;
    }

    /**
     * @param null $FileName
     */
    public function setFileName($FileName): void
    {
        $this->FileName = $FileName;
    }

    /**
     * @return int
     */
    public function getUsernameLen(): int
    {
        return $this->UsernameLen;
    }

    /**
     * @param int $UsernameLen
     */
    public function setUsernameLen(int $UsernameLen): void
    {
        $this->UsernameLen = $UsernameLen;
    }

    /**
     * @return null
     */
    public function getUsername()
    {
        return $this->Username;
    }

    /**
     * @param null $Username
     */
    public function setUsername($Username): void
    {
        $this->Username = $Username;
    }

    /**
     * @return int
     */
    public function getPasswordLen(): int
    {
        return $this->PasswordLen;
    }

    /**
     * @param int $PasswordLen
     */
    public function setPasswordLen(int $PasswordLen): void
    {
        $this->PasswordLen = $PasswordLen;
    }

    /**
     * @return null
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     * @param null $Password
     */
    public function setPassword($Password): void
    {
        $this->Password = $Password;
    }

    /**
     * @return int
     */
    public function getICCIDLen(): int
    {
        return $this->ICCIDLen;
    }

    /**
     * @param int $ICCIDLen
     */
    public function setICCIDLen(int $ICCIDLen): void
    {
        $this->ICCIDLen = $ICCIDLen;
    }

    /**
     * @return null
     */
    public function getICCID()
    {
        return $this->ICCID;
    }

    /**
     * @param null $ICCID
     */
    public function setICCID($ICCID): void
    {
        $this->ICCID = $ICCID;
    }

    /**
     * @return int
     */
    public function getCSQ(): int
    {
        return $this->CSQ;
    }

    /**
     * @param int $CSQ
     */
    public function setCSQ(int $CSQ): void
    {
        $this->CSQ = $CSQ;
    }

    /**
     * @return int
     */
    public function getSER(): int
    {
        return $this->SER;
    }

    /**
     * @param int $SER
     */
    public function setSER(int $SER): void
    {
        $this->SER = $SER;
    }

    /**
     * @return int
     */
    public function getModev(): int
    {
        return $this->Modev;
    }

    /**
     * @param int $Modev
     */
    public function setModev(int $Modev): void
    {
        $this->Modev = $Modev;
    }

    /**
     * @return null
     */
    public function getInet()
    {
        return $this->Inet;
    }

    /**
     * @param null $Inet
     */
    public function setInet($Inet): void
    {
        $this->Inet = $Inet;
    }



}