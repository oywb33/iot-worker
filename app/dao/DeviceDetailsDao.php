<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/27 0027
 * Time: 16:17
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\dao;


class DeviceDetailsDao
{

    public $did = 0;
    public $code_sn = '';
    public $device_id = '';
    public $type_id = 0;
    public $fixed_password = '';
    public $mac = '';
    public $card_num = '';
    public $meid = '';
    public $identifier = 0;
    public $longitude = 0;
    public $latitude = 0;
    public $version = '';
    public $signal = 0;
    public $network = '';
    public $route = '';
    public $alarm_key = '';
    public $heart_cycle = 0;
    public $battery_total = 0;
    public $loanable = '';
    public $refundable = '';
    public $unusual = '';
    public $borrow_power_step = '';
    public $protocol_name = '';
    public $compile_data = '';
    public $compile_time = '';
    public $digital_signal_module = '';
    public $net_hard = '';
    public $net_soft = '';
    public $parked_domain = '';
    public $send_fail = '';
    public $device_verison = '';
    public $device_status = '';
    public $last_power_off_time = 0;
    public $created = 0;
    public $updated = 0;
    public $remask = '';
    public $used_rate = '';

    /**
     * @return int
     */
    public function getDid(): int
    {
        return $this->did;
    }

    /**
     * @param int $did
     */
    public function setDid(int $did): void
    {
        $this->did = $did;
    }

    /**
     * @return string
     */
    public function getCodeSn(): string
    {
        return $this->code_sn;
    }

    /**
     * @param string $code_sn
     */
    public function setCodeSn(string $code_sn): void
    {
        $this->code_sn = $code_sn;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->device_id;
    }

    /**
     * @param string $device_id
     */
    public function setDeviceId(string $device_id): void
    {
        $this->device_id = $device_id;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->type_id;
    }

    /**
     * @param int $type_id
     */
    public function setTypeId(int $type_id): void
    {
        $this->type_id = $type_id;
    }

    /**
     * @return string
     */
    public function getFixedPassword(): string
    {
        return $this->fixed_password;
    }

    /**
     * @param string $fixed_password
     */
    public function setFixedPassword(string $fixed_password): void
    {
        $this->fixed_password = $fixed_password;
    }

    /**
     * @return string
     */
    public function getMac(): string
    {
        return $this->mac;
    }

    /**
     * @param string $mac
     */
    public function setMac(string $mac): void
    {
        $this->mac = $mac;
    }

    /**
     * @return string
     */
    public function getCardNum(): string
    {
        return $this->card_num;
    }

    /**
     * @param string $card_num
     */
    public function setCardNum(string $card_num): void
    {
        $this->card_num = $card_num;
    }

    /**
     * @return string
     */
    public function getMeid(): string
    {
        return $this->meid;
    }

    /**
     * @param string $meid
     */
    public function setMeid(string $meid): void
    {
        $this->meid = $meid;
    }

    /**
     * @return int
     */
    public function getIdentifier(): int
    {
        return $this->identifier;
    }

    /**
     * @param int $identifier
     */
    public function setIdentifier(int $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return int
     */
    public function getLongitude(): int
    {
        return $this->longitude;
    }

    /**
     * @param int $longitude
     */
    public function setLongitude(int $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return int
     */
    public function getLatitude(): int
    {
        return $this->latitude;
    }

    /**
     * @param int $latitude
     */
    public function setLatitude(int $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion(string $version): void
    {
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getSignal(): int
    {
        return $this->signal;
    }

    /**
     * @param int $signal
     */
    public function setSignal(int $signal): void
    {
        $this->signal = $signal;
    }

    /**
     * @return string
     */
    public function getNetwork(): string
    {
        return $this->network;
    }

    /**
     * @param string $network
     */
    public function setNetwork(string $network): void
    {
        $this->network = $network;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute(string $route): void
    {
        $this->route = $route;
    }

    /**
     * @return string
     */
    public function getAlarmKey(): string
    {
        return $this->alarm_key;
    }

    /**
     * @param string $alarm_key
     */
    public function setAlarmKey(string $alarm_key): void
    {
        $this->alarm_key = $alarm_key;
    }

    /**
     * @return string
     */
    public function getHeartCycle(): string
    {
        return $this->heart_cycle;
    }


    /**
     * @param int $heart_cycle
     */
    public function setHeartCycle(int $heart_cycle)
    {
        $this->heart_cycle = $heart_cycle;
    }

    /**
     * @return string
     */
    public function getBatteryTotal(): string
    {
        return $this->battery_total;
    }

    /**
     * @param string $battery_total
     */
    public function setBatteryTotal(string $battery_total): void
    {
        $this->battery_total = $battery_total;
    }

    /**
     * @return string
     */
    public function getLoanable(): string
    {
        return $this->loanable;
    }

    /**
     * @param string $loanable
     */
    public function setLoanable(string $loanable): void
    {
        $this->loanable = $loanable;
    }

    /**
     * @return string
     */
    public function getRefundable(): string
    {
        return $this->refundable;
    }

    /**
     * @param string $refundable
     */
    public function setRefundable(string $refundable): void
    {
        $this->refundable = $refundable;
    }

    /**
     * @return string
     */
    public function getUnusual(): string
    {
        return $this->unusual;
    }

    /**
     * @param string $unusual
     */
    public function setUnusual(string $unusual): void
    {
        $this->unusual = $unusual;
    }

    /**
     * @return string
     */
    public function getBorrowPowerStep(): string
    {
        return $this->borrow_power_step;
    }

    /**
     * @param string $borrow_power_step
     */
    public function setBorrowPowerStep(string $borrow_power_step): void
    {
        $this->borrow_power_step = $borrow_power_step;
    }

    /**
     * @return string
     */
    public function getProtocolName(): string
    {
        return $this->protocol_name;
    }

    /**
     * @param string $protocol_name
     */
    public function setProtocolName(string $protocol_name): void
    {
        $this->protocol_name = $protocol_name;
    }

    /**
     * @return string
     */
    public function getCompileData(): string
    {
        return $this->compile_data;
    }

    /**
     * @param string $compile_data
     */
    public function setCompileData(string $compile_data): void
    {
        $this->compile_data = $compile_data;
    }

    /**
     * @return string
     */
    public function getCompileTime(): string
    {
        return $this->compile_time;
    }

    /**
     * @param string $compile_time
     */
    public function setCompileTime(string $compile_time): void
    {
        $this->compile_time = $compile_time;
    }

    /**
     * @return string
     */
    public function getDigitalSignalModule(): string
    {
        return $this->digital_signal_module;
    }

    /**
     * @param string $digital_signal_module
     */
    public function setDigitalSignalModule(string $digital_signal_module): void
    {
        $this->digital_signal_module = $digital_signal_module;
    }

    /**
     * @return string
     */
    public function getNetHard(): string
    {
        return $this->net_hard;
    }

    /**
     * @param string $net_hard
     */
    public function setNetHard(string $net_hard): void
    {
        $this->net_hard = $net_hard;
    }

    /**
     * @return string
     */
    public function getNetSoft(): string
    {
        return $this->net_soft;
    }

    /**
     * @param string $net_soft
     */
    public function setNetSoft(string $net_soft): void
    {
        $this->net_soft = $net_soft;
    }

    /**
     * @return string
     */
    public function getParkedDomain(): string
    {
        return $this->parked_domain;
    }

    /**
     * @param string $parked_domain
     */
    public function setParkedDomain(string $parked_domain): void
    {
        $this->parked_domain = $parked_domain;
    }

    /**
     * @return string
     */
    public function getSendFail(): string
    {
        return $this->send_fail;
    }

    /**
     * @param string $send_fail
     */
    public function setSendFail(string $send_fail): void
    {
        $this->send_fail = $send_fail;
    }

    /**
     * @return string
     */
    public function getDeviceVerison(): string
    {
        return $this->device_verison;
    }

    /**
     * @param string $device_verison
     */
    public function setDeviceVerison(string $device_verison): void
    {
        $this->device_verison = $device_verison;
    }

    /**
     * @return string
     */
    public function getDeviceStatus(): string
    {
        return $this->device_status;
    }

    /**
     * @param string $device_status
     */
    public function setDeviceStatus(string $device_status): void
    {
        $this->device_status = $device_status;
    }

    /**
     * @return string
     */
    public function getLastPowerOffTime(): string
    {
        return $this->last_power_off_time;
    }

    /**
     * @param int $last_power_off_time
     */
    public function setLastPowerOffTime(int $last_power_off_time): void
    {
        $this->last_power_off_time = $last_power_off_time;
    }

    /**
     * @return int
     */
    public function getCreated(): int
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated(int $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getRemask(): string
    {
        return $this->remask;
    }

    /**
     * @param string $remask
     */
    public function setRemask(string $remask): void
    {
        $this->remask = $remask;
    }

    /**
     * @return string
     */
    public function getUsedRate(): string
    {
        return $this->used_rate;
    }

    /**
     * @param string $used_rate
     */
    public function setUsedRate(string $used_rate): void
    {
        $this->used_rate = $used_rate;
    }



}