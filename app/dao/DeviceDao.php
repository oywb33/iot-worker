<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2021/1/27 0027
 * Time: 16:17
 * Author: oywb
 * Author: iishappyabu@163.com
 */

namespace app\dao;


use app\config\Constant;

class DeviceDao
{
    public $id = 0;
    public $code_sn = '';
    public $device_id = '';
    public $type_id = Constant::DEVICE_TYPE_ID_FOR_SHARK;
    public $fixed_password = '';
    public $card_num = '';
    public $identifier = 0;
    public $longitude = 0;
    public $latitude = 0;
    public $version = '';
    public $signal = '';
    public $network = '';
    public $loanable = 0;
    public $refundable = 0;
    public $serial_number = '';
    public $unusual = 0;
    public $created = 0;
    public $updated = 0;
    public $reg_time = 0;
    public $state = 0;
    public $lock = 0;
    public $sort_id = 0;
    public $device_status = 0;
    public $protocol_name = '';
    public $__table_name = 'cdy_devices';

    /**
     * DeviceDao constructor.
     * @param string $code_sn
     * @param string $device_id
     * @param int $type_id
     * @param int $identifier
     * @param int $state
     */
    public function __construct(string $code_sn, string $device_id, int $type_id, int $identifier=12, int $state=1)
    {
        $this->code_sn = $code_sn;
        $this->device_id = $device_id;
        $this->type_id = $type_id;
        $this->identifier = $identifier;
        $this->state = $state;
        $nowTime = time();
        $this->created = $nowTime;
        $this->updated = $nowTime;
        $this->reg_time = $nowTime;
        $this->signal = 17;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getCodeSn(): string
    {
        return $this->code_sn;
    }

    /**
     * @param string $code_sn
     */
    public function setCodeSn(string $code_sn): void
    {
        $this->code_sn = $code_sn;
    }

    /**
     * @return string
     */
    public function getDeviceId(): string
    {
        return $this->device_id;
    }

    /**
     * @param string $device_id
     */
    public function setDeviceId(string $device_id): void
    {
        $this->device_id = $device_id;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->type_id;
    }

    /**
     * @param int $type_id
     */
    public function setTypeId(int $type_id): void
    {
        $this->type_id = $type_id;
    }

    /**
     * @return string
     */
    public function getFixedPassword(): string
    {
        return $this->fixed_password;
    }

    /**
     * @param string $fixed_password
     */
    public function setFixedPassword(string $fixed_password): void
    {
        $this->fixed_password = $fixed_password;
    }

    /**
     * @return string
     */
    public function getCardNum(): string
    {
        return $this->card_num;
    }

    /**
     * @param string $card_num
     */
    public function setCardNum(string $card_num): void
    {
        $this->card_num = $card_num;
    }

    /**
     * @return int
     */
    public function getIdentifier(): int
    {
        return $this->identifier;
    }

    /**
     * @param int $identifier
     */
    public function setIdentifier(int $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return int
     */
    public function getLongitude(): int
    {
        return $this->longitude;
    }

    /**
     * @param int $longitude
     */
    public function setLongitude(int $longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return int
     */
    public function getLatitude(): int
    {
        return $this->latitude;
    }

    /**
     * @param int $latitude
     */
    public function setLatitude(int $latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion(string $version): void
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getSignal(): string
    {
        return $this->signal;
    }

    /**
     * @param string $signal
     */
    public function setSignal(string $signal): void
    {
        $this->signal = $signal;
    }

    /**
     * @return string
     */
    public function getNetwork(): string
    {
        return $this->network;
    }

    /**
     * @param string $network
     */
    public function setNetwork(string $network): void
    {
        $this->network = $network;
    }

    /**
     * @return int
     */
    public function getLoanable(): int
    {
        return $this->loanable;
    }

    /**
     * @param int $loanable
     */
    public function setLoanable(int $loanable): void
    {
        $this->loanable = $loanable;
    }

    /**
     * @return int
     */
    public function getRefundable(): int
    {
        return $this->refundable;
    }

    /**
     * @param int $refundable
     */
    public function setRefundable(int $refundable): void
    {
        $this->refundable = $refundable;
    }

    /**
     * @return string
     */
    public function getSerialNumber(): string
    {
        return $this->serial_number;
    }

    /**
     * @param string $serial_number
     */
    public function setSerialNumber(string $serial_number): void
    {
        $this->serial_number = $serial_number;
    }

    /**
     * @return int
     */
    public function getUnusual(): int
    {
        return $this->unusual;
    }

    /**
     * @param int $unusual
     */
    public function setUnusual(int $unusual): void
    {
        $this->unusual = $unusual;
    }

    /**
     * @return int
     */
    public function getCreated(): int
    {
        return $this->created;
    }

    /**
     * @param int $created
     */
    public function setCreated(int $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getUpdated(): int
    {
        return $this->updated;
    }

    /**
     * @param int $updated
     */
    public function setUpdated(int $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getRegTime(): int
    {
        return $this->reg_time;
    }

    /**
     * @param int $reg_time
     */
    public function setRegTime(int $reg_time): void
    {
        $this->reg_time = $reg_time;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState(int $state): void
    {
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getLock(): int
    {
        return $this->lock;
    }

    /**
     * @param int $lock
     */
    public function setLock(int $lock): void
    {
        $this->lock = $lock;
    }

    /**
     * @return int
     */
    public function getSortId(): int
    {
        return $this->sort_id;
    }

    /**
     * @param int $sort_id
     */
    public function setSortId(int $sort_id): void
    {
        $this->sort_id = $sort_id;
    }

    /**
     * @return int
     */
    public function getDeviceStatus(): int
    {
        return $this->device_status;
    }

    /**
     * @param int $device_status
     */
    public function setDeviceStatus(int $device_status): void
    {
        $this->device_status = $device_status;
    }

    /**
     * @return string
     */
    public function getProtocolName(): string
    {
        return $this->protocol_name;
    }

    /**
     * @param string $protocol_name
     */
    public function setProtocolName(string $protocol_name): void
    {
        $this->protocol_name = $protocol_name;
    }



}