<?php
/**
 * Copyright (c) 2021. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

namespace app\events;
/**
 * 用于检测业务代码死循环或者长时间阻塞等问题
 * 如果发现业务卡死，可以将下面declare打开（去掉//注释），并执行php start.php reload
 * 然后观察一段时间workerman.log看是否有process_timeout异常
 */
declare(ticks=1);

use app\action\Device;
use app\action\Main;
use app\config\Constant;
use app\config\DeviceConfig;
use app\system\Factory\DBFactory;
use app\system\Factory\RedisFactory;
use app\system\SyLog;
use app\system\SySession;
use \GatewayWorker\Lib\Gateway;
use system\WorkerServer;
use tools\Tools;
use Workerman\Lib\Timer;

/**
 * 主逻辑
 * 主要是处理 onConnect onMessage onClose 三个方法
 * onConnect 和 onClose 如果不需要可以不用实现并删除
 */
class Events
{
    public $connection_count = 0;
    public $host = '0.0.0.0';
    public $port = '8000';
    public $protocol = 'tcp';
    public $_name = 'WmTcp';
    public $workName = '';
    static $sessionFileList = [];

    public static $channelTcpList = [];
    public static $async_connection = null;
    /**
     * 当客户端连接时触发
     * 如果业务不需此回调可以删除onConnect
     * 
     * @param int $client_id 连接id
     */
    public static function onConnect($client_id)
    {
        $gateway_port = isset($_SERVER['GATEWAY_PORT']) ? $_SERVER['GATEWAY_PORT'] : '?';
        $RemoteIp = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '?';
        $RemotePort = isset($_SERVER['REMOTE_PORT']) ? $_SERVER['REMOTE_PORT'] : '?';
        $gateway_addr = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['GATEWAY_ADDR'] : '?';
        outLog('Events::onConnect ->'.$client_id);
        outLog("New Connection ClientID:{$client_id} now RemoteIp:$RemoteIp RemotePort:$RemotePort Gateway_addr:$gateway_addr Gateway_port:$gateway_port ");
    }

    /**
     * 当客户端发来消息时触发
     * @param int $client_id 连接id
     * @param mixed $message 具体消息
     */
    public static function onMessage($client_id, $message)
    {
        outLog('Events::onMessage ->'.$client_id);
    }

    /**
     * 当用户断开连接时触发
     * @param int $client_id 连接id
     */
    public static function onClose($client_id)
    {
        $uid = Gateway::getUidByClientId($client_id);
        outLog(  "Connection closed {$client_id} : uid {$uid}");
        Events::Logout($client_id,$uid);
    }

    public static function Logout($client_id,$uid){
        outLog("uid {$uid} -> client_id {$client_id} Logout");
        $client_list = Events::getClientIdByUid($uid);
        if (!empty($client_list)) {
            $cNum = count($client_list);
            foreach ($client_list as $id) {
                if ($id == $client_id) {//关闭旧的连接
                    $cNum--;
                }
            }

        }

        $connectionSession = SySession::getSession($client_id);
        $timer_list = isset($connectionSession['timer_list']) ? $connectionSession['timer_list'] : [];
        if (isset($timer_list) && !empty($timer_list)) {
            /**
             * 防止无限添加定时器
             */
            foreach ($timer_list as $timer_id) {
                Timer::del($timer_id);
                outLog('Timer::del ' . $timer_id);
            }
        }
        Gateway::closeClient($client_id);
        Events::clearLoginText($client_id);
        SySession::clear($client_id);
        Gateway::unbindUid($client_id, $uid);
    }

    public static function getClientIdByUid($uid)
    {
        $client_info = Gateway::getClientIdByUid($uid);
        if (empty($client_info) or count($client_info) == 0 or $client_info == false) {
            return [];
        }
        return $client_info;
    }

    public static function Login($client_id, $uid)
    {
        if (!empty($uid)) {
            $client_list = Events::getClientIdByUid($uid);
            if (!empty($client_list)) {
                foreach ($client_list as $id) {
                    if ($id != $client_id) {//关闭旧的连接
                        SyLog::print_log($uid,"unbindUid {$uid} client_id {$id}");
                        Gateway::unbindUid($id, $uid);
                        Gateway::closeClient($id);
                    }
                }
            }
            Gateway::bindUid($client_id, $uid);
            SyLog::print_log($uid,"Login {$uid} client_id {$client_id}");
            $connectionSession = Gateway::getSession($client_id);
            $connectionSession['client_id'] = $client_id;
            $connectionSession['uid'] = $uid;
            $connectionSession['login_time'] = Tools::getNowTime();
            unset($connectionSession['auth_timer_id']);
            Gateway::setSession($client_id, $connectionSession);
            SySession::setSession($client_id, $connectionSession);

        }
    }




    public static function saveLoginText($client_id,$message)
    {
        $loginKey = Constant::REDIS_CACHE_DEVICES_LOGIN_INFO_PREFIX;
        if(is_array($message)){
            $message = json_encode($message);
        }
        RedisFactory::getInstance()->hMSet($loginKey, [$client_id => $message]);
        RedisFactory::getInstance()->expire($loginKey, Constant::REDIS_SYSTEM_LOGIN_TOUTIME);
    }

    public static function clearLoginText($client_id){
        $loginKey = Constant::REDIS_CACHE_DEVICES_LOGIN_INFO_PREFIX;
        RedisFactory::getInstance()->hDel($loginKey,$client_id);
    }
    public static function getLoginText($client_id){
        $loginKey = Constant::REDIS_CACHE_DEVICES_LOGIN_INFO_PREFIX;
        $loginCache = RedisFactory::getInstance()->hGet($loginKey,$client_id);
        return $loginCache;
    }

    public static function sendHexToClient($client_id, $sendMsg)
    {
        $uid = Gateway::getUidByClientId($client_id);
        SyLog::print_log($uid, 'client_id:' . $client_id . ' onSend: ' . $sendMsg);
        if (!empty($sendMsg)) {
            Gateway::sendToClient($client_id, hex2bin($sendMsg));
        }
    }

    public static function sendToClient($client_id, $sendMsg)
    {
        $uid = Gateway::getUidByClientId($client_id);
        if (!empty($sendMsg)) {
            Gateway::sendToClient($client_id, $sendMsg);
            $sendMsg = 'client_id:' . $client_id . ' onSend: ' . $sendMsg;
            if(empty($uid)){
                outLog($sendMsg);
            }else{
                SyLog::print_log($uid, $sendMsg);
            }
        }
    }

    public static function onWorkerStart($worker)
    {
        $worker->keep_alive_timer_id = Timer::add(360, function () use ($worker) {
            /**
             * 定时器每30秒定时 "select 1" ，保活。
             */
            DBFactory::getInstance()->query("SELECT 1");
        });
        /**
         * WorkerStart里不能调用getAllClientIdList函数
         */
    }

    public static function onWorkerStop($worker)
    {
        foreach (static::$sessionFileList as $file) {
            if (file_exists($file)) {
                unlink($file);
            }
        }
        SySession::clearSession();
    }

    public static function isEventsTask($message){
        $obj = json_decode($message,true);
        if(isset($obj['client_id']) && isset($obj['task_id'])){
            return true;
        }
        return false;
    }

    public static function msgToTask($message){
        $obj = json_decode($message,true);
        return $obj;
    }

    public static function getClientIdByMsg($task){
        if(isset($task['client_id']) && isset($task['task_id'])){
            return $task['client_id'];
        }
        return 0;
    }
    public static function getTaskId($task){
        if(isset($task['client_id']) && isset($task['task_id'])){
            return $task['task_id'];
        }
        return 0;
    }
}
